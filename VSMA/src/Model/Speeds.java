package Model;

public class Speeds {
	
	private float speedX, speedY, speedXY;
	public float x, y, xy;
	
	public Speeds(float speedX, float speedY, float speedXY){
		this.speedX  = speedX;
		this.speedY  = speedY;
		this.speedXY = speedXY;
		
		x = speedX;
		y = speedY;
		xy = speedXY;
	}
	
	public float getSpeedX() {
		return speedX;
	}
	public float getSpeedY() {
		return speedY;
	}
	public float getSpeedXY() {
		return speedXY;
	}
	
}
