package Model;

import Model.GS;

import java.awt.Color;
import java.util.ArrayList;

public class Team {

	private Color color;
	
	private ArrayList<FootballPlayer> players = new ArrayList<FootballPlayer>();
	
	private ArrayList<Boolean> possession;
	
	public boolean reserv[] = {false, false, false};
	
	public Team(Color c) {

		this.color = c;
		players = new ArrayList<FootballPlayer>();
	}
	
	
	public void addPlayer(FootballPlayer p) {
		
		players.add(p);
		
	}
	
	public FootballPlayer getPlayer(int index) {
		
		return players.get(index);
		
	}
	
	
	public void clear() {
		
		players.clear();
		
	}
	
	public int size() {
		
		return players.size();
		
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setPossession(ArrayList<Boolean> poss) {
		this.possession = poss;
	}
	
	public boolean getPossession(int time) {
		
		if (time < possession.size()) {
			return possession.get(time);
		}
		return false;
		
	}


	public void clearPlayerArea() {
		
		for (int i = 0; i < players.size(); i++) {
			players.get(i).area = 0;
		}
		
	}
	
	public int getArea(int time) {
	
		int total = 0;
		
		for (int i = 0; i < players.size(); i++) {
			total += players.get(i).areas[time];
		}
		
		return total;
	}
	
	public void calculateMainPositions() {
		
		for (int i = 0; i < GS.numberOfPlayers; i++) {
			players.get(i).calculateMainPosition();
		}
		
		//System.out.println(players.get(0).getCenterX() + "\t" + players.get(0).getCenterY());
		
	}
	
}
