package Model;

import Model.GS;



public class Position {

	public float x;
	public float y;
	
	public Position(float x, float y) {
		
		this.x = x;
		this.y = y;
		
		
	}
	
	public Position resized() {
		
		Position p = new Position(0, 0);
		
		p.x = (float) GS.fieldSizeX/GS.realFieldSizeX * x;
		p.y = (float) GS.fieldSizeY/GS.realFieldSizeY * y;
		
		return p;
		
		
	}
	
	
	public float getNormalizedX() {
		
		return GS.realFieldSizeX - x;
				
	}
	
	public float getNormalizedY() {
		
		return GS.realFieldSizeY - y;
				
	}
	
	public Position getNormalized() {
		
		if (this.x < -1000)
			return this;
		
		Position p = new Position(0, 0);
		
		p.x = (float) GS.realFieldSizeX - x;
		p.y = (float) GS.realFieldSizeY - y;
		
		return p;
	}

	public float distanceTo(float i, float j) {
		
		return (float) Math.sqrt((x - i)*(x - i) + (y - j)*(y - j));		
		
	}
	
	public String toString() {
		
		return "(" + x + ", " + y + ")"; 
		
	}
	
}
