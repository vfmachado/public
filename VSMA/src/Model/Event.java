package Model;


public class Event {

	public int frame;
	public int player;
	public Position pos;
	
	public int action;				//enum in the future
	public boolean success;
	
	public int team;
	
	public Event(int frame, int player, Position pos, int action, boolean success){
		
		this.frame = frame;
		this.player = player;
		this.pos = pos;
		
		this.action = action;		//enum in the future
		
		this.success = success;
		
		if (player < 15) 
			team = 0;
		else
			team = 1;
	}
	
	public String toString(){
		return "Frame: "+frame+"  Player: "+player+"   Pos: "+pos.x+","+pos.y+"  Action: "+action+"  Success: "+success;
	}
	
	
}
