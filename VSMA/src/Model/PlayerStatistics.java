package Model;

public class PlayerStatistics {


	/* 		0 - Dom�nio
	   		1 - Passe
			2 - Drible
			3 - Finaliza��o-chute
	   		4 - Finaliza��o-cabeca
	   		5 - Desarme- parte inferior do corpo
	   		6 - Desarme-superior
			7 - Defesa-goleiro
			8 - Fair-play
			9 - Tiro-de-meta
			10 - Lateral
			11 - Escanteio
			12 - Tiro-livre
			13 - Falta
			14 - Gol
			15 - Condu��o
	 */
	
	public int[] actionListSuccess    = {   0,0,0,0,0,
										    0,0,0,0,0,
										    0,0,0,0,0,0 };
	
	public int[] actionListNonSuccess = {   0,0,0,0,0,
											0,0,0,0,0,
											0,0,0,0,0,0 };
	
	
	public void addAction(int action, boolean success){
		
		if (success) {
			actionListSuccess[action]++;
		}
		else {
			actionListNonSuccess[action]++;
		}
		
	}
	
	public float rateOfSuccessEvent(int event) {
		
		if (actionListNonSuccess[event] + actionListSuccess[event] != 0)
			return (float) actionListSuccess[event]/(actionListNonSuccess[event] + actionListSuccess[event]);
		
		return 0;
	}
	
	public float rateOfSuccessEvent(int event1, int event2) {
			
		
		float f1, f2; 
		
		if (actionListNonSuccess[event1] + actionListSuccess[event1] == 0)
			f1 = 0;
		else
			f1 = ((float) (actionListSuccess[event1]))/(actionListNonSuccess[event1] + actionListSuccess[event1]);
		
		if (actionListNonSuccess[event2] + actionListSuccess[event2] == 0)
			f2 = 0;
		else
			f2 = ((float) (actionListSuccess[event2]))/(actionListNonSuccess[event2] + actionListSuccess[event2]);	
		
		
		
		//return (rateOfSuccessEvent(event1) + rateOfSuccessEvent(event2)) /2; 
		if (f1 + f2 == 0)
			return 0;
		
		return ((float) (actionListSuccess[event1] + actionListSuccess[event2]))/(actionListNonSuccess[event1] + actionListSuccess[event1] + actionListNonSuccess[event2] + actionListSuccess[event2]);
		
//		/return (f1 + f2)/2;
		
	}
	
	
}
