package Model;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import Controller.ImportPalette;

public class FootballPlayer {

	
	private int id;
	private Color color;
	private Color pathColor;
	public String name;
	private int myteam;
	
	public int reserv = 0;
	public int reservTime = -1;
	
	private ArrayList<Position> positions = new ArrayList<Position>();
	private ArrayList<Speeds> speeds = new ArrayList<Speeds>();
	
	private boolean triangleUP = false;
	
	public PlayerStatistics statistics = new PlayerStatistics();
	public ArrayList<Event> events = new ArrayList<Event>();
	
	private Font font16 = new Font("Calibri", Font.PLAIN, 16);
	private Font font10 = new Font("Calibri", Font.BOLD, 7);
	
	
	private BufferedImage image = null;
	
	public int area = 0;
	public int [] areas = new int[3000];
	
	public FootballPlayer() {
	
		
	}
	
	
	
	public void setImage(BufferedImage img) {
		this.image = img;
	}
	
	public void setImage(String imageName) {
		
		try {
			image = ImageIO.read(new File(imageName));
			
		}
		catch (IOException e) { 	
			System.out.println("Image for player not found");
		}
		
		
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	public void setID(int id) {
		
		this.id = id;
		
	}
	
	public int getId() {
		return id;
	}
	
	public void setColor(Color c) {
		
		this.color = c;
		
	}
	
public void setPathColor(Color c) {
		
		this.pathColor = c;
		
	}
	
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	public void setMyTeam(int team) {
		this.myteam = team;
	}
	
	public void addPosition(float x, float y) {
		
		positions.add(new Position(x, y));
		
	}
	
	public Position getPosition(int index) {
		
		try {
			return positions.get(index);
		}
		catch (Exception e) {
			return positions.get(positions.size()-1);
		}
		
		
	}
	
	
	public Speeds getSpeed(int index) {
		
		if (index < speeds.size())
			return speeds.get(index);
		else
			return null;
		
	}
	
	//call it after load all data
	public void calculateSpeeds() {
		
		speeds.clear();
		
		Speeds sp;
		
		sp = new Speeds(0,  0,  0);
		speeds.add(sp);
		
		for (int i = 0; i < positions.size() -1; i++) {
			
			float x, y, xy;
			
			x = Math.abs(positions.get(i+1).x - positions.get(i).x);
			y = Math.abs(positions.get(i+1).y - positions.get(i).y);
			xy = (float) Math.sqrt(x * x + y * y); 
			
			sp = new Speeds(x, y, xy);
			speeds.add(sp);
			
		}
		
		
	}
	
	
	private float centerX = 0;
	private float centerY = 0;
	
	public float getCenterX() {
		return this.centerX;
	}

	public float getCenterY() {
		return this.centerY;
	}

	
	public void calculateMainPosition() {
		
		int np = 0;
		
		centerX = 0;
		centerY = 0;
		
		
		for (int i = 0; i < GS.timeInstantMax; i++) {
			
			if (positions.get(i).x >= 0 && positions.get(i).x <= 70) {
				
				if (positions.get(i).y >= 0 && positions.get(i).y <= 105) {
					
					centerX += positions.get(i).x;
					centerY += positions.get(i).y;
					
					np++;
				}
			}
		}
		
			centerX /= np +1;
			centerY /= np +1;
		
		
		//reserva
		if (centerX < 3 && centerY < 3) {
			
			centerX = 65;
			centerY = 10;
		}
		
		
		//System.out.println(centerX + "\t" + centerY);
	}
	
	public void draw(Graphics2D g) {
		g.setFont(font16);
		
		Position p = null;
		//if (GS.timeInstant != 0)
		//	p = positions.get(GS.timeInstant-1).resized();
		//else
			p = positions.get(GS.timeInstant).resized();
		
		
		int cor = ImportPalette.get1DColorBySpeed(speeds.get(GS.timeInstant));
		g.setColor(new Color(cor));
		
		if (GS.timeInstant +1 < GS.timeInstantMax)
			drawTriangle(p, positions.get(GS.timeInstant+1).resized(), g);
		else
			drawTriangle(p, p, g);
		
		//g.fillRect((int)p.x -4 ,(int) p.y -4, 8, 8);
		
		if (GS.selectedPlayer+1 == id && GS.selectedTeam == myteam) {
			g.setColor(Color.white);
			g.setFont(font16);
			g.drawString("" + id, p.x -3, p.y -7);
			g.create();
			
			
		}
		else {
			g.setColor(color);
			g.setFont(font16);
			g.drawString("" + id, p.x -3, p.y -7);
		}
		
		
		if (GS.trackPlayers) {
			
			for (int i = GS.timeInstant; i > GS.timeInstant - GS.trackPlayerSize; i--) {
				
				if (i > GS.timeInstantCurrentMin) {
					Position p2 = positions.get(i - 1).resized();
				
					g.drawLine( (int) p.x, 
								(int) p.y, 
								(int) p2.x, 
								(int) p2.y 
					);
					
					p = positions.get(i -1).resized();
					
				}
			}
			
		}
		
		//if (GS.debug)
		//System.out.println("Player: " + id + "\tPasses " + statistics.rateOfSuccessEvent(1) + "%");
		
	}
	
	public void draw(Graphics2D g, int time, boolean small) {
		g.setFont(font16);
		
		Position p = null;
		//if (time != 0)
		//	p = positions.get(time-1).resized();
		//else
			p = positions.get(time).resized();
		
		
		int cor = ImportPalette.get1DColorBySpeed(speeds.get(time));
		g.setColor(new Color(cor));
		
		if (time +1 < GS.timeInstantMax)
			drawTriangle(p, positions.get(time+1).resized(), g);
		else
			drawTriangle(p, p, g);
		
		//g.fillRect((int)p.x -4 ,(int) p.y -4, 8, 8);
		
		if (GS.selectedPlayer+1 == id && GS.selectedTeam == myteam) {
			g.setColor(Color.white);
			g.setFont(font16);
			g.drawString("" + id, p.x -5, p.y -3);
			//g.create();
			//g.setFont(font16);
			
		}
		else {
			g.setColor(color);
			
			if (!small)
				g.setFont(font10);
			else
				g.setFont(font16);
			
			g.drawString("" + id, p.x -3, p.y -3);
		}
		
		
		if (GS.trackPlayers) {
			
			for (int i = time; i > GS.timeInstant - GS.trackPlayerSize; i--) {
				
				if (i > GS.timeInstantCurrentMin) {
					Position p2 = positions.get(i - 1).resized();
				
					g.drawLine( (int) p.x, 
								(int) p.y, 
								(int) p2.x, 
								(int) p2.y 
					);
					
					p = positions.get(i -1).resized();
					
				}
			}
			
		}
		
		//if (GS.debug)
		//System.out.println("Player: " + id + "\tPasses " + statistics.rateOfSuccessEvent(1) + "%");
		
	}

	private void drawTriangle(Position pf, Position pt, Graphics2D g) {
		
		Position p = new Position(pt.x - pf.x, pt.y - pf.y);
		
		Position p1 = new Position(0, 0);
		Position p2 = new Position(0, 0);
		Position p3 = new Position(0, 0);
		
		
		if (Math.abs(p.x) < 2)
			p.x = 0;
		else if (p.x < 0)
			p.x = -1;
		else
			p.x = 1;
		
		if (Math.abs(p.y) < 2)
			p.y = 0;
		else if (p.y < 0)
			p.y = -1;
		else
			p.y = 1;
		
		//parado
		if (p.x == 0 && p.y == 0) {
			g.fillRect((int) pf.x - 5, (int) pf.y -5, 10, 10);
		}
		else if (p.x == 1 && p.y == 0) {
			p1.x = p2.x = pf.x;
			p1.y = pf.y + 5;
			p2.y = pf.y - 5;
			
			p3.x = pf.x + 15;
			p3.y = pf.y;
			
		}
		else if (p.x == -1 && p.y == 0) {
			p1.x = p2.x = pf.x;
			p1.y = pf.y + 5;
			p2.y = pf.y - 5;
			
			p3.x = pf.x - 15;
			p3.y = pf.y;
		}
		else if (p.x == 0 && p.y == 1) {
			p1.x = pf.x - 5;
			p2.x = pf.x + 5;
			p1.y = pf.y;
			p2.y = pf.y;
			
			p3.x = pf.x ;
			p3.y = pf.y + 15;
		}
		else if (p.x == 0 && p.y == -1) {
			p1.x = pf.x - 5;
			p2.x = pf.x + 5;
			p1.y = pf.y;
			p2.y = pf.y;
			
			p3.x = pf.x ;
			p3.y = pf.y - 15;
		}
		else if (p.x == 1 && p.y == 1) {
	
			p1.x = pf.x - 4;
			p1.y = pf.y + 4;
			
			p2.x = pf.x + 4;
			p2.y = pf.y - 4;
			
			p3.x = pf.x + 10;
			p3.y = pf.y + 10;
			
			
		}
		else if (p.x == -1 && p.y == 1) {
			
			p1.x = pf.x + 4;
			p1.y = pf.y + 4;
			
			p2.x = pf.x - 4;
			p2.y = pf.y - 4;
			
			p3.x = pf.x - 10;
			p3.y = pf.y + 10;
			
		}
		else if (p.x == 1 && p.y == -1) {
			p1.x = pf.x + 4;
			p1.y = pf.y + 4;
			
			p2.x = pf.x - 4;
			p2.y = pf.y - 4;
			
			p3.x = pf.x + 10;
			p3.y = pf.y - 10;
		}
		else if (p.x == -1 && p.y == -1) {
			p1.x = pf.x - 4;
			p1.y = pf.y + 4;
			
			p2.x = pf.x + 4;
			p2.y = pf.y - 4;
			
			p3.x = pf.x - 10;
			p3.y = pf.y - 10;
		}
		
		g.drawLine((int) p1.x, (int) p1.y, (int) p2.x, (int) p2.y); 
		g.drawLine((int) p2.x, (int) p2.y, (int) p3.x, (int) p3.y); 
		g.drawLine((int) p3.x, (int) p3.y, (int) p1.x, (int) p1.y); 
	}
	
	

	public double[][] getHeatData(int t0, int t1) {
		
		double[][]data = new double[401][601];
		
		boolean normalize = false;
		
		if (myteam == 1) {
			if (GS.team1.getPlayer(0).positions.get(0).y < 50) {
				normalize = true;
				//System.out.println("Normalizing for team1");
			}
			else {
				//System.out.println("do not normalize to team 1");
			}
		}
		else if (myteam == 2) {
			if (GS.team2.getPlayer(0).positions.get(0).y < 50) {
				normalize = true;
				//System.out.println("Normalizing for team2");
			}
			else {
				//System.out.println("do not normalize to team 2");
			}
		}
				
		for (int i = t0; i < t1; i++) {
			
			if (GS.onlyPlayingMode)
				if (GS.possession[i] == 3)
					continue;
			
			Position p = new Position(positions.get(i).x, positions.get(i).y);
			
//			if (normalize) {
//				p = p.getNormalized();
//			}
			
			if (p.x >= 0 && p.x <= 70) {
				
				if (p.y >= 0 && p.y <= 105) {
										
					int posx, posy;
					
					posx = (int) p.resized().x;
					posy = (int) p.resized().y;
					
					centerX += p.x;
					centerY += p.y;

					
					try {
					
						for (int px = posx-GS.heatSize; px < posx+GS.heatSize; px++) {
							for (int py = posy-GS.heatSize; py < posy+GS.heatSize; py++) {
								
								double distance = Math.sqrt( (px-posx)* (px-posx) + (py-posy)*(py-posy));
								
								if (distance < GS.heatSize) {
									data[px][py] += 3f/ (distance+4f);
								}
							}
						}

						
					}
					catch (ArrayIndexOutOfBoundsException e) {
						
					}
					
					
					
				}
				
			}
			
		}
		
		
		
		return data;
	}
	
	
	
	public double[][] getHeatDataPossession(boolean yes, int t0, int t1) {
		
		double[][]data = new double[401][601];
		
		
		boolean normalize = false;
		
		if (myteam == 1) {
			if (GS.team1.getPlayer(0).positions.get(0).y < 50) {
				normalize = true;
				System.out.println("Normalizing for team1");
			}
			else {
				System.out.println("do not normalize to team 1");
			}
		}
		else if (myteam == 2) {
			if (GS.team2.getPlayer(0).positions.get(0).y < 50) {
				normalize = true;
				System.out.println("Normalizing for team1");
			}
			else {
				System.out.println("do not normalize to team 2");
			}
		}
				
		
		for (int i = t0; i < t1; i++) {
		
			if (GS.possession[i] != 3) {
				if (yes) { 
					if ( !(GS.possession[i] == myteam) )
						continue;
				}
				else {
					if (GS.possession[i] == myteam) 
						continue;
				}
			}
					
				
			if (GS.onlyPlayingMode)
				if (GS.possession[i] == 3)
					continue;
			
			Position p = new Position(positions.get(i).x, positions.get(i).y);
			
//			if (normalize) {
//				p = p.getNormalized();
//			}
			
			if (p.x >= 0 && p.x <= 70) {
				
				if (p.y >= 0 && p.y <= 105) {
					
					centerX += p.x;
					centerY += p.y;
					
					int posx = (int) p.resized().x;
					int posy = (int) p.resized().y;
					
					try {
					
						for (int px = posx-20; px < posx+20; px++) {
							for (int py = posy-20; py < posy+20; py++) {
								
								double distance = Math.sqrt( (px-posx)* (px-posx) + (py-posy)*(py-posy));
								
								if (distance < 20) {
									data[px][py] += 3f/ (distance+4f);
								}
							}
						}

						
					}
					catch (ArrayIndexOutOfBoundsException e) {
						
					}
					
					
				}
				
			}
				
			
			
		}
		
		
		return data;
	}
	


	public void drawAsIcon(int x, int y, int size, Color c, Graphics2D g) {
		
		
		g.setColor(c);
		g.fillOval(x - size/2, y - size/2, size, size);
		
		
		g.setColor(Color.white);
		g.drawString("" + (this.id), x-5, y+3);
		g.setColor(Color.black);
		g.drawString(this.name, x - 20, y - size/2);
		
		
	}



	public Color getPathColor() {
		
		return this.pathColor;
	}

}
