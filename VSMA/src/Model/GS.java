//CAP COR T2
//http://futpedia.globo.com/campeonato/campeonato-brasileiro/2008/06/29/atletico-pr-1-x-1-coritiba


package Model;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.swing.JCheckBox;

import Controller.DataLoader;

public class GS {
	
	
	public static void loadData(String fileName) {
		
		
		GS.timeInstant = 0;
		
		team1 = new Team(Color.blue);
		team2 = new Team(Color.red);
		
		events = new ArrayList<Event>();
		eventsStr = new ArrayList<String>();
		ballPositions = new ArrayList<Position>();
		
		possTeam1 = new ArrayList<Boolean>();
		
		timesHistogram = new ArrayList<Integer>();
		
		team1.reserv[0] = false;
		team1.reserv[1] = false;
		team1.reserv[2] = false;
		team2.reserv[0] = false;
		team2.reserv[1] = false;
		team2.reserv[2] = false;
		
		if (debug)
			System.out.println("Loading data: " + fileName);
		
		DataLoader dl = new DataLoader();
		
		dl.loadPositions("data/games/" + fileName + ".2d");
		dl.loadEvents("data/games/" + fileName + ".ant");
		dl.loadImages("data/games/" + fileName + ".csv");
		dl.calculateSpeeds();
		
		
		GS.associateEvents();
		
		team1.calculateMainPositions();
		team2.calculateMainPositions();
			
		
		
		forceCPMupdate = true;	//cpm update
		
		timeChange = true;		//tactical update
		
		gamePassChange = true;	//pass update
		timePassChange = true;
		
		controlUpdate = true;	//control panel update time moments
		areaUpdate = true;
		
		
		calculateEventStatistics();
		
		possToTeams();
		
		
	}
	
	public static int team1Poss;
	public static int team2Poss;
	public static int teamNoPoss;
	
	public static boolean inverted = false;
	
	public static boolean controlUpdate = false;
	
	public static boolean timeChange = false;
	
	public static boolean gamePassChange = false; //pass analysis
	public static boolean timePassChange = false;
	
	public static boolean areaUpdate = false;
	
	public static boolean debug = false;
	
	public static String possessionMode = "Roger";
	
	public static boolean tacticalTeam1 = true;
	public static boolean tacticalTeam2 = true;
	public static boolean tacticalPossession = false;
	public static boolean tacticalNonPossession = false;
	public static String tacticalMode = "ProximityMode";
	
	public static ArrayList<String> tacticalT1 = new ArrayList<String>();
	public static ArrayList<String> tacticalT2 = new ArrayList<String>();
	
	public static boolean showTacticalLines;
	public static boolean showTacticalConvex;
	
	public static float frameRate = 30f;
	
	//Application SIZE
	public static int width = 1920;
	public static int height = 1080;
	
	
	//soccer field size (draw)
	public static int fieldSizeX = 400;
	public static int fieldSizeY = 600;
	
	//real soccer field size  70x105
	public static int realFieldSizeX = 70;
	public static int realFieldSizeY = 105;
	
	
	//teams
	public static Team team1 = new Team(Color.blue);
	public static Team team2 = new Team(Color.red);
	
	public static ArrayList<ArrayList<Position>> formationTeam1 = new ArrayList<ArrayList<Position>>();
	public static ArrayList<ArrayList<Position>> formationTeam2 = new ArrayList<ArrayList<Position>>();
	
	//to track player
	public static ArrayList<JCheckBox> boxesTeam1 = new ArrayList<JCheckBox>();
	public static ArrayList<JCheckBox> boxesTeam2 = new ArrayList<JCheckBox>();
	
	//cover timeline
	public static ArrayList<Integer> timesHistogram = new ArrayList<Integer>();
	
	//number of players
	public static int numberOfPlayers = 14;
	
	//events
	public static ArrayList<Event> events = new ArrayList<Event>();
	public static ArrayList<String> eventsStr = new ArrayList<String>();
	
	
	//control time
	public static int timeInstant = 0;
	public static int timeInstantMax = 0;
	
	public static int timeInstantCurrentMin = 0;
	public static int timeInstantCurrentMax = 0;
	
	//selected items	->	-1 for non-selected
	public static int selectedPlayer = 0;
	public static int selectedTeam = 1;
	public static float maxSpeed = 9;
	
	public static boolean trackPlayers = false;
	public static int trackPlayerSize = 6;
	
	public static int [] possession;
	
	
	//HEAT MAP GAUSSIAN SIZE
	public static int heatSize = 20;
	
	
	//CPM
	public static int paletteResolutionX = 34;
	public static int paletteResolutionY = 52;
	
	public static int palette1DResolution = 0;
	
	public static int cpmColumnsSize = 18;
	
	public static boolean forceCPMupdate = false;
	
	
	public static String generator = "Position";
	
	public static boolean cpmZoomMode = false;
	public static boolean onlyPlayingMode = false;
	//public static boolean cpmPossessionMode = false;
	public static boolean cpmAggregation = false;
	
	public static boolean sorted = false;
	
	public static boolean reloadPalette = false;
	
	/************************************************************************/
	
	public static ArrayList<Boolean> possTeam1 = new ArrayList<Boolean>();
	
	//TODO: BUTTON TO SELECT ACTION
	public static int selectedAction = 1;
	
	public static boolean fieldEventsEnabled = false;
	public static boolean fieldCurrentEventEnabled = true;
	public static boolean [] fieldPlayersEnabled = new boolean[2];
	
	//OLD GLOBAL FLAGS
	//TODO SEE IF IT IS THE BEST OPTION
	public static boolean sortCPM_X 	 = false; //0
	public static boolean sortCPM_Y 	 = false; //1
	public static boolean sortPos_X 	 = true; //2
	public static boolean sortPos_Y 	 = true;  //3
	public static boolean success   	 = true;  //4
	public static boolean nonsuccess	 = true;  //5
	public static boolean fteam1    	 = true;  //6
	public static boolean fteam2		 = true;  //7
	public static boolean DrawMode  	 = false; //8
	public static boolean SortMode   	 = true;  //9
	public static boolean NegMode    	 = false; //10
	public static boolean Poss      	 = true;  //11
	public static boolean NoPoss     	 = true;  //12
	public static boolean PlayerPoss     = false; //13
	public static boolean separePossCPM  = false; //14
	public static boolean zoomMode  	 = false; //15
	public static boolean trackMode  	 = true;  //16
	public static boolean eventMode  	 = false; //17
	
	
	public static boolean sortCPM_P		 = false;
	public static boolean sortCPM_T		 = false;
	
	public static boolean[] flagList = { sortCPM_X,sortCPM_Y,sortPos_X,sortPos_Y,success,
										 nonsuccess,fteam1,fteam2,DrawMode,SortMode,NegMode,
										 Poss,NoPoss,PlayerPoss,separePossCPM,zoomMode,trackMode,
										 eventMode};
	
	
	//TYPES OF POSSIBLE EVENTS
	//call it first
	public static void initEventsStrings() {

		GS.eventsStr.clear();
		
		GS.eventsStr.add("0 - Domain");
        GS.eventsStr.add("1 - Pass");
        GS.eventsStr.add("2 - Dribble");
        GS.eventsStr.add("3 - Shots on Goal");
        GS.eventsStr.add("4 - Shots on Goal");
        GS.eventsStr.add("5 - Disarm");
        GS.eventsStr.add("6 - Disarm");
        GS.eventsStr.add("7 - Defense");
        GS.eventsStr.add("8 - Goalkeeper");
        GS.eventsStr.add("9 - Goal Kick");
        GS.eventsStr.add("10 - Lateral");
        GS.eventsStr.add("11 - Corner");
        GS.eventsStr.add("12 - Impediment");
        GS.eventsStr.add("13 - Fault");
        GS.eventsStr.add("14 - Goal");
        GS.eventsStr.add("15 - Conduction");
	}
	
	
	//associate events with players
	//call it after load data
	public static void associateEvents() {
		
		//TODO: OUTRO LUGAR PARA ISTO
		fieldPlayersEnabled[0] = true;
		fieldPlayersEnabled[1] = true;
		
		initEventsStrings();
		
		for (Event e : events) {
		
			
			
			if (e.player <= GS.numberOfPlayers) {
				
				team1.getPlayer(e.player - 1).statistics.addAction(e.action, e.success);
				team1.getPlayer(e.player - 1).events.add(e);
			}
			else {
				if (e.player - GS.numberOfPlayers <= 14) {
					team2.getPlayer(e.player - GS.numberOfPlayers -1).statistics.addAction(e.action, e.success);
					team2.getPlayer(e.player - GS.numberOfPlayers -1).events.add(e);
				}
				else
					System.out.println("Event with player: "  + e.player);
			}
			
		}
		
		
		if (GS.debug)
			System.out.println("Association of events complete");
		
		
		ballEstimation();
		
		
	}
	
	
	public static int convertFrame( int frame) {
		if (frameRate == 30) 
			return frame/30;
		
		else
			return (int) ( (float) frame/7.5f);
	}
	
	public static ArrayList<Position> ballPositions = new ArrayList<Position>();
	
	public static void ballEstimation() {
		
		
		
		Event last = events.get(0);
		Event next;
		int tl = 1;
		
		for (int i = 0; i < GS.timeInstantMax; i++) {
			
			
			if (eventOnTime(i).size() > 0) {
				ballPositions.add(eventOnTime(i).get(0).pos);
				last = eventOnTime(i).get(0);
				tl = i;
			}
			else {
				
				Position p = new Position(0, 0);
				
				next = nextEvt(i);
				
				if (next != null) {
					p.x = last.pos.x + (next.pos.x - last.pos.x) * (i - tl) / (tn - tl);
					p.y = last.pos.y + (next.pos.y - last.pos.y) * (i - tl) / (tn - tl);
					ballPositions.add(p); 
				
				}
			
			}
			
			
		}
		
		
		//System.out.println(ballPositions.size());
		
	}
	
	//recursive next event
	private static int tn = 1;
	public static Event nextEvt(int time) {
		
		if (time >= timeInstantMax)
			return null;
		
		if (eventOnTime(time).size() > 0) {
			tn = time;	
			return eventOnTime(time).get(0);
		}
		else
			return nextEvt(time + 1);
		
	}
	
	
	//get Event on time moment
	public static ArrayList<Event> eventOnTime(int time) {
		
		ArrayList<Event> evts = new  ArrayList<Event>();
		
		if (frameRate == 30) {
		
			for (Event e : events) {
				
				if (e.frame > (time+1)*30) {
					break;
				}
				else {
					
					if (e.frame > (time)*30 && e.frame < (time+1)*30)
						evts.add(e);
					
				}
				
			}
		}
		else {
			
			for (Event e : events) {
				
				if (e.frame > (time+1) * 7.5) {
					break;
				}
				else {
					
					if (e.frame > (time) * 7.5 && e.frame < (time+1) * 7.5)
						evts.add(e);
					
				}
				
			}
		}
		return evts;
		
	}
	
	public static int[] teamAstatistics = new int[16];
	public static int[] teamBstatistics = new int[16];
	public static int[] gameStatistics = new int[16];
	
	public static void calculateEventStatistics() {
		
		//System.out.println("Time instants: " + GS.timeInstantCurrentMin +", "+ GS.timeInstantCurrentMax);
		
		for (int i = 0; i < 16; i++) {
			teamAstatistics[i] = 0;
			teamBstatistics[i] = 0;
			gameStatistics[i] = 0;
		}
		
		for (Event e : GS.events) {
			
			if (e.frame/GS.frameRate > GS.timeInstantCurrentMin && e.frame/GS.frameRate < GS.timeInstantCurrentMax) {
				
				if (e.action == 1 && e.success == true) {
					if (e.team == 0)
						teamAstatistics[e.action]++;
					else
						teamBstatistics[e.action]++;
					
					gameStatistics[e.action]++;
				
				}
				else if (e.action != 1) {
					if (e.team == 0)
						teamAstatistics[e.action]++;
					else
						teamBstatistics[e.action]++;
					
					gameStatistics[e.action]++;
				}
			}
		}
		
	}
	
	public static void possToTeams() {
		
		ArrayList<Boolean> team1poss = new ArrayList<Boolean>();
		ArrayList<Boolean> team2poss = new ArrayList<Boolean>();
		
		for (int i = 0; i < possession.length; i++) {
			
			if (possession[i] == 1) {
				team1poss.add(true);
				team2poss.add(false);
			}
			
			else if (possession[i] == 2) {
				team1poss.add(false);
				team2poss.add(true);
			}
			
			else {
				team1poss.add(false);
				team2poss.add(false);
			}
		}
		
		team1.setPossession(team1poss);
		team2.setPossession(team2poss);
	}

	
	public static void generateVTKFile(int[] pts, Color[] colors) {
		
		PrintWriter writer, writer2;
		try {
			writer = new PrintWriter("jogadores.txt", "UTF-8");
			//writer2 = new PrintWriter("Team2.vtk", "UTF-8");
			
//			writer.println("DATASET POLYDATA");
//			writer.println("POINTS 2700 float");
			
//			writer2.println("DATASET POLYDATA");
//			writer2.println("POINTS 2700 float");
			
			//for (int i = 0; i < 11; i++) {
				int i = 7;
				FootballPlayer fp = GS.team1.getPlayer(i);
				FootballPlayer fp2 = GS.team2.getPlayer(i);
				
				for (int t = 0; t < 2700; t++) {
					
					writer.println("" + fp.getPosition(t).resized().x +" " + fp.getPosition(t).resized().y + " " + t);
					//writer2.println("" + fp2.getPosition(t).resized().x +" " + fp2.getPosition(t).resized().y + " " + t);
				}
			//}
			
			
//			writer.println("LINES 11 29700");
//			//writer2.println("LINES 11 29700");
//			//
//			for (int i = 0; i < 11; i++) {
//				writer.print("2700 ");
//				//writer2.print("2700 ");
//				for (int t = 0; t < 2700-1; t++) {
//					
//					writer.print( (2700*i+t) + " ");
//					//writer2.print( (2700*i+t) + " ");
//				}
//				writer.println();
//				writer2.println();
//			}
			
			
//			writer.println("CELLS 11 2700 ");
//			writer2.println("CELLS 11 2700");
//			
//			//for (int i = 0; i < 11; i++) {
//								
//				for (int t = 0; t < 2700; t++) {
//					
//					writer.println(t);
//					writer2.println(t);
//				}
//			//}
//			
//			
//			writer.println("CELL_TYPES 11");
//			writer2.println("CELL_TYPES 11");
//			for (int i = 0; i < 11; i++) {
//				writer.println("4");
//				writer2.println("4");
//			}
								
			writer.close();
			//writer2.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("VTK Gerado!");
	}
	

}





