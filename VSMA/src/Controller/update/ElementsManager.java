package Controller.update;

import java.util.ArrayList;

public class ElementsManager {
	
	
	private static ArrayList<MyElement> elements = new ArrayList<MyElement>();
	

	public static void addElement(MyElement element) {
		
		elements.add(element);
		
	}
	
	public static void notifyElements() {

		for (MyElement element : elements) {
			element.update();
		}
		
	}
	
	
}
