package Controller;

import Model.GS;
import Model.Speeds;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;


public class ImportPalette {

	public static ArrayList<ArrayList<Integer>> palette2D_pos;
	
	public static ArrayList<ArrayList<Integer>> palette2D_drawMode;
	
	public static ArrayList<Integer> palette1D_speed;
	
	public static ArrayList<ArrayList<Boolean>> palette2DActivePosition;
	public static ArrayList<ArrayList<Boolean>> palette2DDrawPosition;
	
	public static BufferedImage palette2dImg;
	
	
	public ArrayList<ArrayList<Integer>> fillPalette2D_pos(String palette2d_end) {
		
		//open image
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(palette2d_end));
			palette2dImg = img;
		}
		catch (IOException e) {
			System.out.println("Palette 2D Image not found");
		}
		
		//dimensions defined by GS
		int dimX = GS.paletteResolutionX;
		int dimY = GS.paletteResolutionY;
		
		//relation between img and dim
		//roger uses floor (int) cast... 
		//ill try different to improve precision
		float finalGranX = (float) img.getWidth() / dimX;
		float finalGranY = (float) img.getHeight() / dimY;
		
		ArrayList<Integer> palette1D = new ArrayList<Integer>(dimX);
		palette2D_pos = new ArrayList<>(dimY);
		
		for (int y = 0; y < dimY; y++) {
			
			for (int x = 0; x < dimX; x++) {
				
				int color = img.getRGB( (int) (x * finalGranX) ,
										(int) (y * finalGranY)  
									  );
				
				palette1D.add(x, color);
				
			}
			
			palette2D_pos.add(y, palette1D);
			
			palette1D = new ArrayList<Integer>(dimX);
			
		}
		
		
		return palette2D_pos;
	}

	
	public static ArrayList<ArrayList<Integer>> fillPalette2D_drawMode(String paletteDrawMode) {
		
		//open image
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(paletteDrawMode));
			//palette2dImg = img;
		}
		catch (IOException e) {
			System.out.println("Palette Draw Mode 2D Image not found");
		}
		
		//dimensions defined by GS
		int dimX = GS.paletteResolutionX;
		int dimY = GS.paletteResolutionY;
		
		//relation between img and dim
		//roger uses floor (int) cast... 
		//ill try different to improve precision
		float finalGranX = (float) img.getWidth() / dimX;
		float finalGranY = (float) img.getHeight() / dimY;
		
		ArrayList<Integer> palette1D = new ArrayList<Integer>(dimX);
		palette2D_drawMode = new ArrayList<>(dimY);
		
		for (int y = 0; y < dimY; y++) {
			
			for (int x = 0; x < dimX; x++) {
				
				int color = img.getRGB( (int) (x * finalGranX) ,
										(int) (y * finalGranY)  
									  );
				
				palette1D.add(x, color);
				
			}
			
			palette2D_drawMode.add(y, palette1D);
			
			palette1D = new ArrayList<Integer>(dimX);
			
		}
		
		
		return palette2D_drawMode;
		
	}

	//private int speedDimX;
	public ArrayList<Integer> fillPalette1D_speed(String palette1d_speed_end) {

		//open image
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(palette1d_speed_end));
		}
		catch (IOException e) {
			System.out.println("Palette 1D Speed - Image not found");
		}
			
		int dimX = Integer.parseInt(palette1d_speed_end.substring(33, 35));
		
		GS.palette1DResolution = dimX;
		
		float finalGranX = (float) img.getWidth() / dimX;
		
		palette1D_speed = new ArrayList<Integer>(dimX);
		
		for(int x = 0; x < dimX; x++){
			
			int color = img.getRGB(
									(int) (x * finalGranX),
									img.getHeight()/2
								  );
			
			palette1D_speed.add(x,color);
		}
	
		return palette1D_speed;
		
	}
	
	
	public static void fillPalette2DActivePosition() {
		
		int dimX = GS.paletteResolutionX; 
		int dimY = GS.paletteResolutionY;
		
		ArrayList<Boolean> palette1DAux = new ArrayList<Boolean>(dimX);
		palette2DActivePosition = new ArrayList<>(dimY);
		
		for(int y = 0; y < dimY; y++) {
		
			for(int x = 0; x < dimX; x++) {
				palette1DAux.add(x,false);
			}
			
			palette2DActivePosition.add(palette1DAux);
			palette1DAux  = new ArrayList<Boolean>(dimX);
		}
		
	}
	
	
	
	public static int get2DColorByCoordinates(int paletteX, int paletteY) {
		 
		if( paletteX >= 0 && paletteX < GS.paletteResolutionX 
							&&
		   paletteY >= 0 && paletteY < GS.paletteResolutionY
		  ) {
			
			if (GS.DrawMode == true) {
				return  palette2D_drawMode.get(paletteY).get(paletteX);
			}
			else {
				return  palette2D_pos.get(paletteY).get(paletteX);
			}
		}
		else {
			System.out.println("ERROR: ... ImportPalette class >> get2DColorByCoordinates()");
			return 0;
		}
		
	}

	public static int get1DColorBySpeed(Speeds speed){
		
		float distValue = 0f;
		//10 m/s
		if(GS.flagList[2] && GS.flagList[3]){
			distValue = speed.getSpeedXY();
		}
		else if(GS.flagList[2]){
			distValue = speed.getSpeedX();
		}
		else if(GS.flagList[3]){
			distValue = speed.getSpeedY();
		}
		
		//TODO: melhorar essa normalização com a speed;
		int value  = (int) Math.round( (Math.log(distValue*distValue+1)) * ( (float) GS.palette1DResolution / (GS.maxSpeed/2) ) );
		//int value  = (int) Math.round( distValue * (float) GS.palette1DResolution / (GS.maxSpeed) ) ;
		
		if (value >= 0)
			return palette1D_speed.get( value > GS.palette1DResolution-1 ? GS.palette1DResolution-1 : value );
		else
			return palette1D_speed.get(0);
	}
	

}
