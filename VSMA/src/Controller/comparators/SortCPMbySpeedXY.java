//Chamada:  
//
//ArrayList<AverageController> listAvg = new ArrayList<AverageController>(); 
//Collections.sort(listAvg, new SortListAvg());
  
 
//Classe:

package Controller.comparators;
import java.util.Comparator;

import Model.Speeds;



public class SortCPMbySpeedXY implements Comparator<Speeds> {
	public int compare(Speeds result1, Speeds result2) {
		
		/*
		Double val1 = (double) result1.x+result1.y*1.00001;
		Double val2 = (double) result2.x+result2.y*1.00001;//*1.00001 para o caso de desempate

		// the "compareTo" method is part of the Comparable interface, and
		// provides a means of fully ordering objects.
		return val1.compareTo(val2);
		// return result1.getMetricValue() - result2.getMetricValue();
		
		 */
		float val1 = result1.xy;
		float val2 = result2.xy;
		if (val1 > val2) 
			return 1;
		else if (val1 == val2)
			return 0;
		else
			return -1;
		
	}
}
