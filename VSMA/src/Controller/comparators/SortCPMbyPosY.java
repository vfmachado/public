


//Chamada:  
//
//ArrayList<AverageController> listAvg = new ArrayList<AverageController>(); 
//Collections.sort(listAvg, new SortListAvg());
  
 
//Classe:

package Controller.comparators;
import java.util.Comparator;

import Model.Position;

public class SortCPMbyPosY implements Comparator<Position> {
	
	@Override
	public int compare(Position p0, Position p1) {
		
		Double val1 = (double) p0.y;
		Double val2 = (double) p1.y;

		
		if (val1 < -1000)
			val1 = 9999.123;
		if (val2 < -1000)
			val2 = 9999.123;
		
			
		// the "compareTo" method is part of the Comparable interface, and
		// provides a means of fully ordering objects.
		return val1.compareTo(val2);
	}

}
	