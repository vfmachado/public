


//Chamada:  
//
//ArrayList<AverageController> listAvg = new ArrayList<AverageController>(); 
//Collections.sort(listAvg, new SortListAvg());
  
 
//Classe:

package Controller.comparators;
import java.util.Comparator;

import Model.Speeds;


public class SortCPMbySpeedX implements Comparator<Speeds> {
	public int compare(Speeds result1, Speeds result2) {
		Double val1 = (double) result1.getSpeedX();
		Double val2 = (double) result2.getSpeedX();

		// the "compareTo" method is part of the Comparable interface, and
		// provides a means of fully ordering objects.
		return val1.compareTo(val2);
		// return result1.getMetricValue() - result2.getMetricValue();
	}
}
