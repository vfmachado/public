


//Chamada:  
//
//ArrayList<AverageController> listAvg = new ArrayList<AverageController>(); 
//Collections.sort(listAvg, new SortListAvg());
  
 
//Classe:

package Controller.comparators;
import java.util.Comparator;

import Model.Position;


public class SortCPMbyPosX implements Comparator<Position> {
	public int compare(Position result1, Position result2) {
		Double val1 = (double) result1.x;
		Double val2 = (double) result2.x;

		
		if (val1 < -1000)
			val1 = 9999.123;
		if (val2 < -1000)
			val2 = 9999.123;
		
			
		// the "compareTo" method is part of the Comparable interface, and
		// provides a means of fully ordering objects.
		return val1.compareTo(val2);
		// return result1.getMetricValue() - result2.getMetricValue();
	}
}
