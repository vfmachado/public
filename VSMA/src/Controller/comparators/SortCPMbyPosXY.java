//Chamada:  
//
//ArrayList<AverageController> listAvg = new ArrayList<AverageController>(); 
//Collections.sort(listAvg, new SortListAvg());
  
 
//Classe:

package Controller.comparators;
import java.util.Comparator;

import Model.Position;



public class SortCPMbyPosXY implements Comparator<Position> {
	public int compare(Position result1, Position result2) {
		
		/*
		Double val1 = (double) result1.x+result1.y*1.00001;
		Double val2 = (double) result2.x+result2.y*1.00001;//*1.00001 para o caso de desempate

		// the "compareTo" method is part of the Comparable interface, and
		// provides a means of fully ordering objects.
		return val1.compareTo(val2);
		// return result1.getMetricValue() - result2.getMetricValue();
		
		 */
		Double val1, val2;
		
		
		
		if (result1.x < -1000 || result1.x > 1000)
			return 1;
		if (result2.x < -1000 || result2.x > 1000)
			return -1;
		
		//val1 = (double) result1.x > -1000 ? -result1.x+result1.y*1.00001 : result1.x+result1.y*1.00001  ;
		//val2 = (double) result2.x > -1000 ? -result2.x+result2.y*1.00001 : result2.x+result2.y*1.00001  ;//*1.00001 para o caso de desempate

		if (result1.x > -1000 && result1.x < 1000) {
			val1 = -result1.x+result1.y*2.00001;
		}
		else
			val1 = result1.x+result1.y*2.00001;
		
		if (result2.x > -1000 && result2.x < 1000) {
			val2 = -result2.x+result2.y*2.00001;
		}
		else
			val2 = result2.x+result2.y*2.00001;
		
		
		if (val1 > val2) 
			return -1;
		else if (val1 == val2)
			return 0;
		else
			return 1;
		
	}
}
