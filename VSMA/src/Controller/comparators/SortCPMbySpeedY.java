


//Chamada:  
//
//ArrayList<AverageController> listAvg = new ArrayList<AverageController>(); 
//Collections.sort(listAvg, new SortListAvg());
  
 
//Classe:

package Controller.comparators;
import java.util.Comparator;

import Model.Speeds;

public class SortCPMbySpeedY implements Comparator<Speeds> {
	
	@Override
	public int compare(Speeds p0, Speeds p1) {
		
		if (p0.y < p1.y)
			return 1;
		else if (p0.y == p1.y) {
			return 0;			
		}
		else
			return -1;
	}

}
