package Controller;


import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import Model.Event;
import Model.FootballPlayer;
import Model.GS;
import Model.Position;


public class DataLoader {

	public void loadPositions(String filename) {
		
		//clear - for reloads
		GS.team1.clear();
		GS.team2.clear();
		
		//create players into teams
		FootballPlayer newPlayer;
        for (int i = 0; i < GS.numberOfPlayers; i++) {
        	
        	newPlayer = new FootballPlayer();
        	newPlayer.setID(i + 1);
        	newPlayer.setColor(Color.blue);
            
        	GS.team1.addPlayer(newPlayer);
            GS.team1.getPlayer(i).setMyTeam(1);
        	
        	newPlayer = new FootballPlayer();
        	newPlayer.setID(i + 1);
        	newPlayer.setColor(Color.red);
        	
        	GS.team2.addPlayer(newPlayer);
        	GS.team2.getPlayer(i).setMyTeam(2);
        }
		
        
        try {
        	
        	FileReader fr = new FileReader(filename);
        	LineNumberReader lnr = new LineNumberReader(fr);
        	
        	
        	//count lines
        	try {
        		lnr.skip(Long.MAX_VALUE);
        	} catch (IOException e) {
        		JOptionPane.showMessageDialog(null, "IO Exception - Contact the support: fritzenvinicius@gmail.com");
        	}
        	
        	int lines = lnr.getLineNumber();
        	lnr.close();
        	
        	//put the number of frames into GS controller
        	GS.timeInstantMax = lines -1;
        	GS.timeInstantCurrentMax = lines -1;
        	
        	if (GS.debug)
        		System.out.println("Time Instants: " +GS.timeInstantMax);
        	
        	//reopen file =/
        	fr = new FileReader(filename);
        	Scanner in = new Scanner(fr);
        	
        	//position Y, X for each player;
        	float x, y;
        	int frame = 0;
        	String trash;
        	
        	
        	if (in.hasNext()) {
        		
        		GS.frameRate = Float.parseFloat(in.next());
        		
        		frame = in.nextInt();
        		y = Float.parseFloat(in.next());
        		
        		//first Y is the goal keeper position, 
        		//so it define the direction of atack of each team
        		
        		
        		//reload file again =/
        		//finally load positions
        		fr.close();
        		fr = new FileReader(filename);
        		in = new Scanner(fr);

        		float trashFrameRate = Float.parseFloat(in.next());
        		int oldframe = 0;
        		if (y < 50) {
        			GS.inverted = false;
        			while (in.hasNext()) {
        				
        				
        				frame = in.nextInt();
        				
        				for (int i = 0; i < GS.numberOfPlayers; i++) {
	        				y = Float.parseFloat(in.next());
	                        x = Float.parseFloat(in.next());
	                        GS.team1.getPlayer(i).addPosition(x, y);
	                        if (frame > oldframe+1)
	                        	 GS.team1.getPlayer(i).addPosition(x, y);
        				}
        				
        				for (int i = 0; i < GS.numberOfPlayers; i++) {
	        				y = Float.parseFloat(in.next());
	                        x = Float.parseFloat(in.next());
	                        GS.team2.getPlayer(i).addPosition(x, y);
	                        if (frame > oldframe+1)
	                        	 GS.team2.getPlayer(i).addPosition(x, y);
        				}
                        
        				oldframe = frame;
        				if (in.hasNextLine())
        					trash = in.nextLine();
        			}
        			
        		}
        		else {
        			GS.inverted = true;
        			while (in.hasNext()) {
        				
        				frame = in.nextInt();
        				
        				for (int i = 0; i < GS.numberOfPlayers; i++) {
	        				y = Float.parseFloat(in.next());
	                        x = Float.parseFloat(in.next());
	                        GS.team2.getPlayer(i).addPosition(x, y);
	                        if (frame > oldframe+1)
	                        	 GS.team2.getPlayer(i).addPosition(x, y);
        				}
        				
        				for (int i = 0; i < GS.numberOfPlayers; i++) {
	        				y = Float.parseFloat(in.next());
	                        x = Float.parseFloat(in.next());
	                        GS.team1.getPlayer(i).addPosition(x, y);
	                        if (frame > oldframe+1)
	                        	 GS.team1.getPlayer(i).addPosition(x, y);
        				}
        				oldframe = frame;
        				if (in.hasNextLine())
        					trash = in.nextLine();
        			}
        			
        			
        		}
        		
        	}
        	
        	GS.timeInstantMax = frame;
        	GS.timeInstantCurrentMax = frame;
        	
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "DataFile didn't found: " + filename);
        } catch (IOException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
	}
	
	
	public void loadEvents(String filename) {
		
		GS.events.clear();
		
		FileReader fr;
		try {
			
			fr = new FileReader(filename);
			BufferedReader br = new BufferedReader(fr);
			
			int frame = 0, player, action;
			float x, y;
			boolean success;
			Position pos;
			
			String line = br.readLine();
			String[] tokens = line.split(" ");
			
			while(line != null){
				
				tokens = line.split(" ");
		
				frame = Integer.parseInt(tokens[0]);
				player = (int) Float.parseFloat(tokens[1]);
				
	    		y = Float.parseFloat(tokens[2]);
	    		x = Float.parseFloat(tokens[3]);
	    	
	    		pos = new Position(x, y);
	    		
	    		action  = (int) Float.parseFloat(tokens[4]);
	    		
	    		if ((int) Float.parseFloat(tokens[5]) == 0)
	    			success = false;
	    		else 
	    			success = true;
	    		
	    		GS.events.add(new Event(frame, player, pos, action, success));
			
	    		line = br.readLine();	  
			}
			
			if (GS.debug)
				System.out.println("Last Event Frame: " + frame);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		//POSSESSION
		GS.possession = new int[GS.timeInstantMax];
        
        //System.out.println(GS.possession.length);
        int index = 0;
        int stoppedGame = 0;
        
        
        for (Event e : GS.events) {
        	
        	int evtCode = e.action;
        	int team = e.team;
        	
        	if (evtCode >= 9 && evtCode <= 14) {
        		GS.possession[GS.convertFrame(e.frame)] = 3;
        	}
        	else {
        		GS.possession[GS.convertFrame(e.frame)] = team+1;
        	}
        	       	
        	
        }
        
        
//        for (int i = 0; i < GS.possession.length; i++) {
//        	
//        	int evtCode = GS.events.get(index).action;
//        	int success = GS.events.get(index).success == true ? 1: 0;
//        	int team =  GS.events.get(index).team;
//        	
//        	        	
//        	if (evtCode >= 9 && evtCode <= 14) {
//        		GS.possession[i] = 3;
//        		stoppedGame++;
//        	}
//        	else {
//        		GS.possession[i] = team;
//        	}
//        	
//        	
//        	//if (index < GS.possession.length -2) {
//        	if (index < GS.events.size() -1) {
//        		int nextIndex = (int) ( GS.events.get(index+1).frame/GS.frameRate );
//        		
//        		int j;
//        		
//        		for (j = i+1; j < nextIndex ; j++) {
//        			GS.possession[j] = GS.possession[j-1];
//        		}
//        		
//        		i = j;
//        		
//        	}
//        	else
//        		break;
//        	
//        	index++;
//        	
//        }
//        
//        System.out.println("Interrupções no jogo: " + stoppedGame);
        
        //completa a posse para os frames sem eventos
        int start = 0;
        for (int i = 0; i < GS.possession.length; i++) {
        	if (GS.possession[i] != 0) {
        		start = i;
        		break;
        	}
        }
        
        int next;
        for (int i = start+1; i < GS.possession.length; i++) {
        	
        	if (GS.possession[i] == 0)
        		GS.possession[i] = GS.possession[i-1];
        	
        	
        }
        
        
        
        int team1 = 0, team2 = 0, stopped = 0;
        for (int i = 0; i < GS.timeInstantMax; i++) {
        	
        	if (GS.possession[i] == 1)
        		team1++;
        	else if (GS.possession[i] == 2)
        		team2++;
        	else if (GS.possession[i] == 3)
        		stopped++;
        	//System.out.println(GS.events.get(i).team);
        	
        }
        //if (GS.debug)
        GS.team1Poss = team1;
        GS.team2Poss = team2;
        GS.teamNoPoss = stopped;
        	System.out.println("Team1\tTeam2\tStopped\n" + team1 + "\t" + team2 + "\t" + stopped);
        
	}
	
	public void calculateSpeeds() {
		
		for (int i = 0; i < GS.numberOfPlayers; i++) {
			
			GS.team1.getPlayer(i).calculateSpeeds();
			GS.team2.getPlayer(i).calculateSpeeds();
		}
		
	}


	public void loadImages(String filename) {
		
		
		try {
        	
        	FileReader fr = new FileReader(filename);
        	Scanner in = new Scanner(fr);
        	
                	
        	for (int i = 0; i < GS.numberOfPlayers; i++) {
        		
        		String[] strs = in.nextLine().split(Pattern.quote(","));
        	
        		
        		GS.team1.getPlayer(i).setName(strs[0]);
        		GS.team1.getPlayer(i).setImage(strs[1]);
        		
        	}
        	
        	for (int i = 0; i < GS.numberOfPlayers; i++) {
        		
        		String[] strs = in.nextLine().split(Pattern.quote(","));

        		        		
        		GS.team2.getPlayer(i).setName(strs[0]);
        		GS.team2.getPlayer(i).setImage(strs[1]);
        		
        	}
        	
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "DataFile didn't found: " + filename);
        } catch (IOException ex) {
            Logger.getLogger(DataLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
	
		
		
	}
	
	
}
