package Controller;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;

import Controller.comparators.SortCPMbyPosX;
import Controller.comparators.SortCPMbyPosXY;
import Controller.comparators.SortCPMbyPosY;
import Controller.comparators.SortCPMbySpeedX;
import Controller.comparators.SortCPMbySpeedXY;
import Controller.comparators.SortCPMbySpeedY;
import Model.FootballPlayer;
import Model.GS;
import Model.Position;
import Model.Speeds;
import Model.Team;

public class VisuGenerator {
	
	//actual cpm for CPM class interactive
	public static String actualCPM;
	
	public int cpmSize = 0;
	
	//data copy for sorted cpms	
	private ArrayList<ArrayList<Position>> copyDataT1;
	private ArrayList<ArrayList<Position>> copyDataT2;
	
	private ArrayList<ArrayList<Speeds>> copySpeedDataT1;
	private ArrayList<ArrayList<Speeds>> copySpeedDataT2;
	
	
	//first methods - generated initial cpms based on GS.generator ("Position" or "Speed")
	public void generateCPMs() {
		
		//cpm images		
		BufferedImage cpmTeam1 = null;
		BufferedImage cpmTeam2 = null;
		
		//update actual cpm
		actualCPM = GS.generator;
		
		
		//if (GS.generator == "Position") {
			
			if (GS.debug)
				System.out.println("CPM position generating");
			
			//call it for each team
			cpmTeam1 = generateByPoss_PositionCPM(GS.team1);	//Position mode
			cpmTeam2 = generateByPoss_PositionCPM(GS.team2);
			
		//}
		
		if (GS.generator == "Speed") {
			
			if (GS.debug)
				System.out.println("CPM speed generating");
			
			//call it for each team
			cpmTeam1 = generateByPoss_SpeedCPM(GS.team1);		//Speed mode
			cpmTeam2 = generateByPoss_SpeedCPM(GS.team2);
		}
		
		
		//update cpms on CPM class		
		CPM.team1CPM = cpmTeam1;
		CPM.team2CPM = cpmTeam2;
		
		//TODO: CLONE THIS OBJECTS
		CPM.team1CPM_genuine = cpmTeam1;
		CPM.team2CPM_genuine = cpmTeam2;
				
		
		
		//when sort
		if (GS.sorted)
			generateSortedCPM();
		
		//visual possession timeline
		//createPossessionVisualLine();
		
		
	}
	
	
//	//create possession timeline - nothing to change for a while
//	public void createPossessionVisualLine(){
//				
//		int columnNum = 10,
//			nextFrame = GS.events.get(0).frame,
//			iterator = 0;
//		
//		//CORES
//		Color cTeam1 = Color.white;
//		Color cTeam2 = Color.white;
//		
//		Color colorPossession = Color.green.darker();
//		Color colorDefense = Color.pink;
//		
//		
//		//IMAGENS
//		BufferedImage team1 = new BufferedImage(
//				columnNum,
//				GS.timeInstantCurrentMax,	 //- GS.timeInstantCurrentMin,
//				BufferedImage.TYPE_INT_RGB
//			 );
//		
//		BufferedImage team2 = new BufferedImage(
//				columnNum,
//				GS.timeInstantCurrentMax, 	//- GS.timeInstantCurrentMin,
//				BufferedImage.TYPE_INT_RGB
//			 );
//		
//		
//		BufferedImage nteam1 = new BufferedImage(
//				columnNum,
//				GS.timeInstantCurrentMax,	 //- GS.timeInstantCurrentMin,
//				BufferedImage.TYPE_INT_RGB
//			 );
//		
//		BufferedImage nteam2 = new BufferedImage(
//				columnNum,
//				GS.timeInstantCurrentMax, 	//- GS.timeInstantCurrentMin,
//				BufferedImage.TYPE_INT_RGB
//			 );
//		
//		
//		//LIMPA
//		GS.possTeam1.clear();
//		
//		boolean team1Possession = true;
//		
//		//PARA O INTERVALO DE TEMPO ESTABELECIDO
//		for (int y = GS.timeInstantCurrentMin; y < GS.timeInstantCurrentMax; y++) {
//			
//			ArrayList<Event> evts = GS.eventOnTime(y);
//			
//			//if (GS.debug)
//			//	System.out.println("Events size: " + evts.size());
//			
//			for (Event e : evts) {
//				
//			
//				if (e.action == 0) {
//					
//						//update teamColors
//						if (evts.get(0).team == 0){
//							cTeam1 = colorPossession;
//							cTeam2 = colorDefense;
//							team1Possession = true;
//						}
//						else {
//							cTeam1 = colorDefense;
//							cTeam2 = colorPossession;
//							team1Possession = false;
//						}
//						
//				}
//					
//			}
//			
//			GS.possTeam1.add(team1Possession);
//					
//			for(int x = 0; x < 10; x++){
//				team1.setRGB(x, y, cTeam1.getRGB());
//				team2.setRGB(x, y, cTeam2.getRGB());
//			}
//			
//			for(int x = 0; x < 10; x++){
//				
//				if (GS.possession[y] == 1) {
//					nteam1.setRGB(x, y, colorPossession.getRGB());
//					nteam2.setRGB(x, y, colorDefense.getRGB());	
//				}
//				else if (GS.possession[y] == 2) {
//					nteam2.setRGB(x, y, colorPossession.getRGB());
//					nteam1.setRGB(x, y, colorDefense.getRGB());	
//				}
//				else {
//					nteam2.setRGB(x, y, Color.white.getRGB());
//					nteam1.setRGB(x, y, Color.white.getRGB());	
//				}
//				
//			}
//			
//							
//			
//		}
//		
//		GS.team1.setPossession(GS.possTeam1);
//		
//		ArrayList<Boolean> poss2 = new ArrayList<Boolean>();
//		
//		for (int i = 0; i < GS.possTeam1.size(); i++) {
//			if (GS.possTeam1.get(i) == true) {
//				poss2.add(false);
//			}
//			else {
//				poss2.add(true);
//			}
//		}
//		
//		GS.team2.setPossession(poss2);
//		
//		
//		if (GS.possessionMode == "Roger") {
//			CPM.possessionTeam1 = team1;
//			CPM.possessionTeam2 = team2;
//		}
//		else if (GS.possessionMode == "Vinicius") {
//			CPM.possessionTeam1 = nteam1;
//			CPM.possessionTeam2 = nteam2;
//		}
//
//	}
	
	
	public void reloadCPMs() {
		
		//TODO - REAMOSTRAGEM DO CPM DE ACORDO COM O INTERVALO DE TEMPO SELECIONADO
		
	}
	
	

	public BufferedImage generateByPoss_PositionCPM(Team t) {
		
		//image for cpm
		BufferedImage cpm = new BufferedImage(
												11 * GS.cpmColumnsSize,
												GS.timeInstantMax,	// - GS.timeInstantCurrentMin,
												BufferedImage.TYPE_INT_RGB
											 );
		
		int[] coloredLine;	//line for each time instant
		int dy = 0;
		for (int y = 0; y < GS.timeInstantMax; y++) {
		
			coloredLine = getLineRepresentation(t, y);
			
			for (int x = 0; x < coloredLine.length; x++) {
				
				if (!GS.onlyPlayingMode) {
					cpm.setRGB(x, dy, coloredLine[x]);
					
				}
				else {
					if (GS.possession[y] != 3) {
						cpm.setRGB(x, dy, coloredLine[x]);
					}
					else {
						cpm.setRGB(x, dy, Color.white.getRGB());
					}
					
				}
				
				try {
					if (coloredLine[x] == Color.black.getRGB()) {
						cpm.setRGB(x, dy-1, coloredLine[x]);
						cpm.setRGB(x, dy-2, coloredLine[x]);
						cpm.setRGB(x, dy-3, coloredLine[x]);
						cpm.setRGB(x, dy-4, coloredLine[x]);
					}
						
				}
				catch (Exception e) {
					System.out.println("Exce��o conhecida e OK");
				}
				
			}
			
//			if (GS.cpmPossessionMode && GS.possession[y] != 3)
//				dy++;
//			else
//				dy++;
			
			if (!GS.onlyPlayingMode)
				dy++;
			else {
				if (!GS.cpmAggregation)
					dy++;
				else {
					if (GS.possession[y] != 3)
						dy++;
				}
					
			}
			
			
		}
		
		cpmSize = dy;
		return cpm;
		
	}
	 
	

	
	public BufferedImage generateByPoss_SpeedCPM(Team team1) {
		
		BufferedImage cpm = new BufferedImage(
				11 * GS.cpmColumnsSize,
				GS.timeInstantMax,	// - GS.timeInstantCurrentMin,
				BufferedImage.TYPE_INT_RGB
			 );

		int[] coloredLine;
		int dy = 0;
		for (int y = 0; y < GS.timeInstantMax; y++) {
		
			coloredLine = getSpeedLineRepresentation(team1, y);
			
			for (int x = 0; x < coloredLine.length; x++) {
				
				if (!GS.onlyPlayingMode) {
					cpm.setRGB(x, dy, coloredLine[x]);
					
				}
				else {
					if (GS.possession[y] != 3)
						cpm.setRGB(x, dy, coloredLine[x]);
						
					else
						cpm.setRGB(x, dy, Color.white.getRGB());
				}
				
				
				try {
					if (coloredLine[x] == Color.black.getRGB()) {
						cpm.setRGB(x, dy-1, coloredLine[x]);
						cpm.setRGB(x, dy-2, coloredLine[x]);
						cpm.setRGB(x, dy-3, coloredLine[x]);
						cpm.setRGB(x, dy-4, coloredLine[x]);
					}
						
				}
				catch (Exception e) {
					System.out.println("Exce��o conhecida e OK");
				}
				
			}
			
			if (!GS.onlyPlayingMode)
				dy++;
			else {
				if (!GS.cpmAggregation)
					dy++;
				else {
					if (GS.possession[y] != 3)
						dy++;
				}
			}
		}
				
		cpmSize = dy;
		return cpm;
				
	}


	
	public void generateSortedCPM() {
		
		//copy all data to a matrix

		copyDataT1 = new ArrayList<ArrayList<Position>>();
		copyDataT2 = new ArrayList<ArrayList<Position>>();
		
		copySpeedDataT1 = new ArrayList<ArrayList<Speeds>>();
		copySpeedDataT2 = new ArrayList<ArrayList<Speeds>>();
		 
		ArrayList<Position> auxiliar1;
		ArrayList<Position> auxiliar2;
		
		ArrayList<Speeds> auxiliarSpeed1;
		ArrayList<Speeds> auxiliarSpeed2;
		
		
		for (int i = 0; i < GS.timeInstantMax; i++) {
			
			if (GS.onlyPlayingMode) {
				if (GS.possession[i] == 3)
					continue;
			}
			
			auxiliar1 = new ArrayList<Position>();
			auxiliar2 = new ArrayList<Position>();
			
			auxiliarSpeed1 = new ArrayList<Speeds>();
			auxiliarSpeed2 = new ArrayList<Speeds>();
			
			
//			for (int j = 0; j < GS.numberOfPlayers; j++) {
//				
//				if (GS.team1.getPlayer(0).getPosition(0).y > GS.realFieldSizeY/2) {
//					auxiliar1.add(new Position(GS.team1.getPlayer(j).getPosition(i).x, GS.team1.getPlayer(j).getPosition(i).y));
//					auxiliarSpeed1.add(new Speeds(	GS.team1.getPlayer(j).getSpeed(i).getSpeedX(), 
//													GS.team1.getPlayer(j).getSpeed(i).getSpeedY(), 
//													GS.team1.getPlayer(j).getSpeed(i).getSpeedXY()));
//				}
//				else {
//					auxiliar1.add(new Position(GS.team1.getPlayer(j).getPosition(i).getNormalizedX(), GS.team1.getPlayer(j).getPosition(i).getNormalizedY()));
//					auxiliarSpeed1.add(new Speeds(	GS.team1.getPlayer(j).getSpeed(i).getSpeedX(), 
//							GS.team1.getPlayer(j).getSpeed(i).getSpeedY(), 
//							GS.team1.getPlayer(j).getSpeed(i).getSpeedXY()));
//				}
//				if (GS.team2.getPlayer(0).getPosition(0).y > GS.realFieldSizeY/2) {
//					auxiliar2.add(new Position(GS.team2.getPlayer(j).getPosition(i).x, GS.team2.getPlayer(j).getPosition(i).y));
//					auxiliarSpeed2.add(new Speeds(	GS.team2.getPlayer(j).getSpeed(i).getSpeedX(), 
//							GS.team2.getPlayer(j).getSpeed(i).getSpeedY(), 
//							GS.team2.getPlayer(j).getSpeed(i).getSpeedXY()));
//				}
//				else {
//					auxiliar2.add(new Position(GS.team2.getPlayer(j).getPosition(i).getNormalizedX(), GS.team2.getPlayer(j).getPosition(i).getNormalizedY()));
//					auxiliarSpeed2.add(new Speeds(	GS.team2.getPlayer(j).getSpeed(i).getSpeedX(), 
//							GS.team2.getPlayer(j).getSpeed(i).getSpeedY(), 
//							GS.team2.getPlayer(j).getSpeed(i).getSpeedXY()));
//				}
//				
//								
//			}
			
			//para os 11 jogadores
			for (int j = 0; j < 11; j++) {
																//TEAM 1
				//se ele esta atacando de baixo para cima
				if (GS.team1.getPlayer(0).getPosition(0).y > GS.realFieldSizeY/2) {
					
					//se nao tem reserva ou se nao chegou a hora que o reserva entra
					if (GS.team1.getPlayer(j).reserv == 0 || GS.team1.getPlayer(j).reservTime > i) {
						
						//copia a posicao
						auxiliar1.add(new Position(GS.team1.getPlayer(j).getPosition(i).x, GS.team1.getPlayer(j).getPosition(i).y));
						
						//copia a velocidade
						auxiliarSpeed1.add(new Speeds(	GS.team1.getPlayer(j).getSpeed(i).getSpeedX(), 
														GS.team1.getPlayer(j).getSpeed(i).getSpeedY(), 
														GS.team1.getPlayer(j).getSpeed(i).getSpeedXY()));
					}
					//se for o reserva
					else {
						FootballPlayer reserv = GS.team1.getPlayer(GS.team1.getPlayer(j).reserv);
						//copia a posicao do reserva
						auxiliar1.add(new Position(reserv.getPosition(i).x, reserv.getPosition(i).y));
						
						//copia a velocidade do reserva
						auxiliarSpeed1.add(new Speeds(	reserv.getSpeed(i).getSpeedX(), 
														reserv.getSpeed(i).getSpeedY(), 
														reserv.getSpeed(i).getSpeedXY()));
					}
				}
				
				//se esta atacando de cima para baixo (invertido) - precisa usar o getNormalized
				else {
					//se nao tem reserva ou se nao chegou a hora que o reserva entra
					if (GS.team1.getPlayer(j).reserv == 0 || GS.team1.getPlayer(j).reservTime > i) {
					
						auxiliar1.add(new Position(GS.team1.getPlayer(j).getPosition(i).getNormalizedX(), GS.team1.getPlayer(j).getPosition(i).getNormalizedY()));
						auxiliarSpeed1.add(new Speeds(	GS.team1.getPlayer(j).getSpeed(i).getSpeedX(), 
								GS.team1.getPlayer(j).getSpeed(i).getSpeedY(), 
								GS.team1.getPlayer(j).getSpeed(i).getSpeedXY()));
					}
					else {
						FootballPlayer reserv = GS.team1.getPlayer(GS.team1.getPlayer(j).reserv);
						//copia a posicao do reserva
						auxiliar1.add(new Position(reserv.getPosition(i).getNormalizedX(), reserv.getPosition(i).getNormalizedY()));
						
						//copia a velocidade do reserva
						auxiliarSpeed1.add(new Speeds(	reserv.getSpeed(i).getSpeedX(), 
														reserv.getSpeed(i).getSpeedY(), 
														reserv.getSpeed(i).getSpeedXY()));
					}
				}
				
																//TEAM 2
				//se ele esta atacando de baixo para cima
				if (GS.team2.getPlayer(0).getPosition(0).y > GS.realFieldSizeY/2) {

					//se nao tem reserva ou se nao chegou a hora que o reserva entra
					if (GS.team2.getPlayer(j).reserv == 0 || GS.team2.getPlayer(j).reservTime > i) {

						//copia a posicao
						auxiliar2.add(new Position(GS.team2.getPlayer(j).getPosition(i).x, GS.team2.getPlayer(j).getPosition(i).y));

						//copia a velocidade
						auxiliarSpeed2.add(new Speeds(	GS.team2.getPlayer(j).getSpeed(i).getSpeedX(), 
								GS.team2.getPlayer(j).getSpeed(i).getSpeedY(), 
								GS.team2.getPlayer(j).getSpeed(i).getSpeedXY()));
					}
					//se for o reserva
					else {
						FootballPlayer reserv = GS.team2.getPlayer(GS.team2.getPlayer(j).reserv);
						//copia a posicao do reserva
						auxiliar2.add(new Position(reserv.getPosition(i).x, reserv.getPosition(i).y));

						//copia a velocidade do reserva
						auxiliarSpeed2.add(new Speeds(	reserv.getSpeed(i).getSpeedX(), 
								reserv.getSpeed(i).getSpeedY(), 
								reserv.getSpeed(i).getSpeedXY()));
					}
				}

				//se esta atacando de cima para baixo (invertido) - precisa usar o getNormalized
				else {
					//se nao tem reserva ou se nao chegou a hora que o reserva entra
					if (GS.team2.getPlayer(j).reserv == 0 || GS.team2.getPlayer(j).reservTime > i) {

						auxiliar2.add(new Position(GS.team2.getPlayer(j).getPosition(i).getNormalizedX(), GS.team2.getPlayer(j).getPosition(i).getNormalizedY()));
						auxiliarSpeed2.add(new Speeds(	GS.team2.getPlayer(j).getSpeed(i).getSpeedX(), 
								GS.team2.getPlayer(j).getSpeed(i).getSpeedY(), 
								GS.team2.getPlayer(j).getSpeed(i).getSpeedXY()));
					}
					else {
						FootballPlayer reserv = GS.team2.getPlayer(GS.team2.getPlayer(j).reserv);
						//copia a posicao do reserva
						auxiliar2.add(new Position(reserv.getPosition(i).getNormalizedX(), reserv.getPosition(i).getNormalizedY()));

						//copia a velocidade do reserva
						auxiliarSpeed2.add(new Speeds(	reserv.getSpeed(i).getSpeedX(), 
								reserv.getSpeed(i).getSpeedY(), 
								reserv.getSpeed(i).getSpeedXY()));
					}
				}

				
				
			}

			copyDataT1.add(auxiliar1);
			copyDataT2.add(auxiliar2);
			
			copySpeedDataT1.add(auxiliarSpeed1);
			copySpeedDataT2.add(auxiliarSpeed2);
			
		}
		
		
		if (GS.debug)
			System.out.println("Copy data complete for sorted CPMs");
		
		
		if (GS.sortCPM_P) {
				
			if (GS.sortCPM_Y && GS.sortCPM_X) {
				
				System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
				
				for (int i = 0; i < copyDataT1.size(); i++) {
	
					Collections.sort(copyDataT1.get(i), new SortCPMbyPosXY());
					Collections.sort(copyDataT2.get(i), new SortCPMbyPosXY());
					
					Collections.sort(copySpeedDataT1.get(i), new SortCPMbySpeedXY());
					Collections.sort(copySpeedDataT2.get(i), new SortCPMbySpeedXY());
					
				}
				
			}
			else if (GS.sortCPM_X) {
				
				for (int i = 0; i < copyDataT1.size(); i++) {
					
					Collections.sort(copyDataT1.get(i), new SortCPMbyPosX());
					Collections.sort(copyDataT2.get(i), new SortCPMbyPosX());
					
					Collections.sort(copySpeedDataT1.get(i), new SortCPMbySpeedX());
					Collections.sort(copySpeedDataT2.get(i), new SortCPMbySpeedX());
	
				}
				
			}
			
			else if (GS.sortCPM_Y) {
				
				for (int i = 0; i < copyDataT1.size(); i++) {
	
					Collections.sort(copyDataT1.get(i), new SortCPMbyPosY());
					Collections.sort(copyDataT2.get(i), new SortCPMbyPosY());
					
					Collections.reverse(copyDataT1.get(i));
					Collections.reverse(copyDataT2.get(i));
					
					Collections.sort(copySpeedDataT1.get(i), new SortCPMbySpeedY());
					Collections.sort(copySpeedDataT2.get(i), new SortCPMbySpeedY());
					
					Collections.reverse(copySpeedDataT1.get(i));
					Collections.reverse(copySpeedDataT2.get(i));
					
				}
			}
		}
		
		if (GS.sortCPM_T) {
			
			
			if (GS.sortCPM_Y && GS.sortCPM_X) {
				
				
				ArrayList<ArrayList<Position>> copyDataT12 = new ArrayList<ArrayList<Position>>();
				ArrayList<ArrayList<Position>> copyDataT22 = new ArrayList<ArrayList<Position>>();
				
				ArrayList<ArrayList<Speeds>> copyDataSpeedT12 = new ArrayList<ArrayList<Speeds>>();
				ArrayList<ArrayList<Speeds>> copyDataSpeedT22 = new ArrayList<ArrayList<Speeds>>();
				
				copyDataT12 = transpose(copyDataT1);
				copyDataT22 = transpose(copyDataT2);
				
				copyDataSpeedT12 = transposeSpeed(copySpeedDataT1);
				copyDataSpeedT22 = transposeSpeed(copySpeedDataT2);
				
				System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
				for (int i = 0; i < 11; i++) {
	
					Collections.sort(copyDataT12.get(i), new SortCPMbyPosXY());
					Collections.sort(copyDataT22.get(i), new SortCPMbyPosXY());
					
					Collections.sort(copyDataSpeedT12.get(i), new SortCPMbySpeedXY());
					Collections.sort(copyDataSpeedT22.get(i), new SortCPMbySpeedXY());
				}
							
				copyDataT1 = transpose(copyDataT12);
				copyDataT2 = transpose(copyDataT22);
				
				copySpeedDataT1 = transpose(copyDataSpeedT12);
				copySpeedDataT2 = transpose(copyDataSpeedT22);
				
			}
			else if (GS.sortCPM_X) {
				
				ArrayList<ArrayList<Position>> copyDataT12 = new ArrayList<ArrayList<Position>>();
				ArrayList<ArrayList<Position>> copyDataT22 = new ArrayList<ArrayList<Position>>();
				
				ArrayList<ArrayList<Speeds>> copyDataSpeedT12 = new ArrayList<ArrayList<Speeds>>();
				ArrayList<ArrayList<Speeds>> copyDataSpeedT22 = new ArrayList<ArrayList<Speeds>>();
				
				copyDataT12 = transpose(copyDataT1);
				copyDataT22 = transpose(copyDataT2);
				
				copyDataSpeedT12 = transposeSpeed(copySpeedDataT1);
				copyDataSpeedT22 = transposeSpeed(copySpeedDataT2);
				
				System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
				for (int i = 0; i < 11; i++) {
	
					Collections.sort(copyDataT12.get(i), new SortCPMbyPosX());
					Collections.sort(copyDataT22.get(i), new SortCPMbyPosX());
					
					Collections.sort(copyDataSpeedT12.get(i), new SortCPMbySpeedX());
					Collections.sort(copyDataSpeedT22.get(i), new SortCPMbySpeedX());
				}
							
				copyDataT1 = transpose(copyDataT12);
				copyDataT2 = transpose(copyDataT22);
				
				copySpeedDataT1 = transpose(copyDataSpeedT12);
				copySpeedDataT2 = transpose(copyDataSpeedT22);
				
			}
			
			else if (GS.sortCPM_Y) {
				
				ArrayList<ArrayList<Position>> copyDataT12 = new ArrayList<ArrayList<Position>>();
				ArrayList<ArrayList<Position>> copyDataT22 = new ArrayList<ArrayList<Position>>();
				
				ArrayList<ArrayList<Speeds>> copyDataSpeedT12 = new ArrayList<ArrayList<Speeds>>();
				ArrayList<ArrayList<Speeds>> copyDataSpeedT22 = new ArrayList<ArrayList<Speeds>>();
				
				copyDataT12 = transpose(copyDataT1);
				copyDataT22 = transpose(copyDataT2);
				
				copyDataSpeedT12 = transposeSpeed(copySpeedDataT1);
				copyDataSpeedT22 = transposeSpeed(copySpeedDataT2);
				
				System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
				for (int i = 0; i < 11; i++) {
	
					Collections.sort(copyDataT12.get(i), new SortCPMbyPosY());
					Collections.sort(copyDataT22.get(i), new SortCPMbyPosY());
					
					Collections.reverse(copyDataT12.get(i));
					Collections.reverse(copyDataT22.get(i));
				
					
					Collections.sort(copyDataSpeedT12.get(i), new SortCPMbySpeedY());
					Collections.sort(copyDataSpeedT22.get(i), new SortCPMbySpeedY());
					
					Collections.reverse(copyDataSpeedT12.get(i));
					Collections.reverse(copyDataSpeedT22.get(i));
				
				}
							
				copyDataT1 = transpose(copyDataT12);
				copyDataT2 = transpose(copyDataT22);
				
				copySpeedDataT1 = transpose(copyDataSpeedT12);
				copySpeedDataT2 = transpose(copyDataSpeedT22);
					
						
			}
		}
		cpmSorted();
		
		
	}
	
	private void cpmSorted() {
		
		BufferedImage cpm1 = new BufferedImage(
		
				11 * GS.cpmColumnsSize,
				copyDataT1.size()+1,	// - GS.timeInstantCurrentMin,
				BufferedImage.TYPE_INT_RGB
			 );
		
		BufferedImage cpm2 = new BufferedImage(
				
				11 * GS.cpmColumnsSize,
				copyDataT1.size()+1,	// - GS.timeInstantCurrentMin,
				BufferedImage.TYPE_INT_RGB
			 );

		int[] coloredLine;
		
		
		
		if (GS.generator == "Position") {
			
			for (int y = 0; y < copyDataT1.size(); y++) {
				
				coloredLine = getLineRepresentationInSorted1(GS.team1, y);
				
				for (int x = 0; x < coloredLine.length; x++) {
				
					cpm1.setRGB(x, y, coloredLine[x]); 
				
				}	
				
			}
			
			for (int y = 0; y < copyDataT1.size(); y++) {
				
				coloredLine = getLineRepresentationInSorted2(GS.team2, y);
				
				for (int x = 0; x < coloredLine.length; x++) {
				
					cpm2.setRGB(x, y, coloredLine[x]); 
				
				}	
				
			}
		}
		
		else if (GS.generator == "Speed") {
			//TODO
			for (int y = 0; y < copyDataT1.size(); y++) {
				
				coloredLine = getSpeedLineRepresentationSorted1(GS.team1, y);
				
				for (int x = 0; x < coloredLine.length; x++) {
				
					cpm1.setRGB(x, y, coloredLine[x]); 
				
				}	
				
			}
			
			for (int y = 0; y < copyDataT1.size(); y++) {
				
				coloredLine = getSpeedLineRepresentationSorted2(GS.team2, y);
				
				for (int x = 0; x < coloredLine.length; x++) {
				
					cpm2.setRGB(x, y, coloredLine[x]); 
				
				}	
				
			}
		}
		
		
		CPM.sortedTeam1CPM = cpm1;
		CPM.sortedTeam2CPM = cpm2;
		
	}
	
	
	private int[] getLineRepresentationInSorted1(Team t, int timeMoment) {
		
		int[] line = new int[11 * GS.cpmColumnsSize];
		
		Position pos;
	
		boolean inverted = true;
		
		/*
		if(t.getPlayer(0).getPosition(0).y > GS.realFieldSizeY/2) inverted = true;
		else 									    inverted = false;  
		*/
				
		for (int i = 0; i < 11; i++) {
			
			if(t.getPlayer(0).getPosition(0).y < GS.realFieldSizeY/2)
				pos = copyDataT1.get(timeMoment).get(i).getNormalized();
			else
				pos = copyDataT1.get(timeMoment).get(i);
				
			pos = copyDataT1.get(timeMoment).get(i);
			
			for (int pix = 0; pix < GS.cpmColumnsSize; pix++) {
				if (pos.y < -1000 || pos.y > 1000)
					line[(i * GS.cpmColumnsSize) + pix] = Color.white.getRGB();
				else
					line[(i * GS.cpmColumnsSize) + pix] = getPositionRespective2DPaletteColor(pos.x,pos.y, inverted);
					
			}
			
		}
		
		
		return line;
		
	}
	
	
	private int[] getLineRepresentationInSorted2(Team t, int timeMoment) {
		
		int[] line = new int[11 * GS.cpmColumnsSize];
		
		Position pos;
	
		boolean inverted = true;
		
		/*
		if(t.getPlayer(0).getPosition(0).y > GS.realFieldSizeY/2) inverted = true;
		else 									    inverted = false;  
		*/
	
		
		for (int i = 0; i < 11; i++) {
			
			
			if(t.getPlayer(0).getPosition(0).y < GS.realFieldSizeY/2)
				pos = copyDataT2.get(timeMoment).get(i).getNormalized();
			else
				pos = copyDataT2.get(timeMoment).get(i);
			
			pos = copyDataT2.get(timeMoment).get(i);
			
			for (int pix = 0; pix < GS.cpmColumnsSize; pix++) {
				if (pos.y < -1000 || pos.y > 1000)
					line[(i * GS.cpmColumnsSize) + pix] = Color.white.getRGB();
				else
					line[(i * GS.cpmColumnsSize) + pix] = getPositionRespective2DPaletteColor(pos.x,pos.y, inverted);
				
			}
			
		}
		
		
		return line;
		
	}
	
	
	private int[] getLineRepresentation(Team t, int timeMoment) {
		
		int[] line = new int[11 * GS.cpmColumnsSize];
		
		Position pos;
	
		boolean inverted = true;
		
		/*
		
		if(t.getPlayer(0).getPosition(0).y > GS.realFieldSizeY/2) inverted = true;
		else				 									    inverted = false;	  
		
		*/
		
		
		int reservEnter = -1;
		
		//for (int i = 0; i < GS.numberOfPlayers; i++) {
		//apenas 11 jogador agora
		for (int i = 0; i < 11; i++) {		
			//se o jogador nao tem reserva
			if (t.getPlayer(i).reserv == 0) {
				//se o jogador esta fora do campo
				if (t.getPlayer(i).getPosition(timeMoment).y < -1000 && t.getPlayer(i).getPosition(timeMoment+60).y < -1000) {
					//para cada um dos 3 reservas
					for (int r = 0;  r < 3; r++) {
						//se o reserva nao foi utilizado
						if (t.reserv[r])
							continue;
						//se o reserva esta no campo depois de um tempo (1 minuto)
						else if (t.getPlayer(11+r).getPosition(timeMoment+60).y < -1000) {
							continue;
						}
						//associa o reserva ao jogador
						else {
							t.getPlayer(i).reserv = r+11;
							t.getPlayer(i).reservTime = timeMoment;
							t.reserv[r] = true;
							
							//System.out.println("Reserva " + (r+11) + " associado ao jogador " + i + "\n\t no instante " + timeMoment);
							//System.out.println("Posicao registrada no momento que o reserva entrou: " + t.getPlayer(i).getPosition(timeMoment).toString());
							
							reservEnter = i;
							
							break;
						}
							
					}
				}
			}
			
			//se o jogador nao tem reserva ou ele ainda nao entrou
			if (t.getPlayer(i).reserv == 0 || t.getPlayer(i).reservTime > timeMoment) {
				//se o time esta atacando de forma invertida
				if(t.getPlayer(0).getPosition(0).y < GS.realFieldSizeY/2)
					pos = t.getPlayer(i).getPosition(timeMoment).getNormalized();		//salva a posicao
				else
					pos = t.getPlayer(i).getPosition(timeMoment);						//salva a posicao
			}
			//busca no reserva a posi��o
			else {
				//se o time esta atacando de forma invertida
				if(t.getPlayer(0).getPosition(0).y < GS.realFieldSizeY/2)
					pos = t.getPlayer(t.getPlayer(i).reserv).getPosition(timeMoment).getNormalized();		//salva a posicao
				else
					pos = t.getPlayer(t.getPlayer(i).reserv).getPosition(timeMoment);						//salva a posicao
			}
			for (int pix = 0; pix < GS.cpmColumnsSize; pix++) {
				
				if (t.getPlayer(i).reservTime == timeMoment) {
					line[(i * GS.cpmColumnsSize) + pix] = Color.black.getRGB();
					
				}
				else if (pos.y < -1000 || pos.y > 1000)
					line[(i * GS.cpmColumnsSize) + pix] = Color.white.getRGB();
				else
					line[(i * GS.cpmColumnsSize) + pix] = getPositionRespective2DPaletteColor(pos.x,pos.y, inverted);
				
								
			}
			
			
			
		}
		
		//line[(11 * GS.cpmColumnsSize)] = Color.black.getRGB();
		
		
		return line;
		
	}
	
	private int[] getSpeedLineRepresentation(Team t, int timeMoment) {
		
		int[] line = new int[11 * GS.cpmColumnsSize];
		
		Speeds speed;
		
		try {
		
			for (int i = 0; i < 11; i++) {
				
				if (t.getPlayer(i).reserv != 0 && timeMoment > t.getPlayer(i).reservTime) {
					speed = t.getPlayer(t.getPlayer(i).reserv).getSpeed(timeMoment);
				}
				else {
					speed = t.getPlayer(i).getSpeed(timeMoment);
				}
				
				for (int pix = 0; pix < GS.cpmColumnsSize; pix++){
					line[(i * GS.cpmColumnsSize) + pix] = ImportPalette.get1DColorBySpeed(speed);
					
					if (t.getPlayer(i).reserv != 0 && timeMoment == t.getPlayer(i).reservTime) {
						line[(i * GS.cpmColumnsSize) + pix] = Color.black.getRGB();
					}
				}
			}
		
		}
		catch(Exception e) {
			
		}
	
		return line;
	}
	
	private int[] getSpeedLineRepresentationSorted1(Team t, int timeMoment) {
		
		int[] line = new int[11 * GS.cpmColumnsSize];
		
		Speeds speed;
		
		for (int i = 0; i < 11; i++) {
			
			//speed = t.getPlayer(i).getSpeed(timeMoment);
			speed = copySpeedDataT1.get(timeMoment).get(i);
			for (int pix = 0; pix < GS.cpmColumnsSize; pix++){
				line[(i * GS.cpmColumnsSize) + pix] = ImportPalette.get1DColorBySpeed(speed);
			}
		}
	
		return line;
	}
	
	private int[] getSpeedLineRepresentationSorted2(Team t, int timeMoment) {
		
		int[] line = new int[11 * GS.cpmColumnsSize];
		
		Speeds speed;
		
		for (int i = 0; i < 11; i++) {
			
			//speed = t.getPlayer(i).getSpeed(timeMoment);
			speed = copySpeedDataT2.get(timeMoment).get(i);
			for (int pix = 0; pix < GS.cpmColumnsSize; pix++){
				line[(i * GS.cpmColumnsSize) + pix] = ImportPalette.get1DColorBySpeed(speed);
			}
		}
	
		return line;
	}
	
	
	//public int getPositionRespective2DPaletteColor(float x, float y, boolean inverted){
	public int getPositionRespective2DPaletteColor(float x, float y, boolean inverted) {	
		int paletteX, paletteY;
		float fieldPropX = GS.realFieldSizeX;
		float fieldPropY = GS.realFieldSizeY; 
		
		//fix outliers
		if(x < 0)
			x = 0;
		else if (x > fieldPropX)
			x = fieldPropX;
		
		if(y < 0) 
			y=0;
		else if (y > fieldPropY)
			y = fieldPropY;
		
		
		//map position to palette (normalize)
		paletteX = (int) (GS.paletteResolutionX*x/fieldPropX);
		paletteY = (int) (GS.paletteResolutionY*y/fieldPropY);
		
		
		paletteX = GS.paletteResolutionX - paletteX;
		paletteY = GS.paletteResolutionY - paletteY;
		
		//fix remaining outliers		
		if(paletteX >= GS.paletteResolutionX)
			paletteX -= 1;
		
		if(paletteY >= GS.paletteResolutionY) 
			paletteY -= 1;
		
		
		if (inverted) {
		if (GS.DrawMode) {
			if (inverted) {
				paletteX = ImportPalette.palette2D_drawMode.get(0).size() - paletteX - 1;
				paletteY = ImportPalette.palette2D_drawMode.size() - paletteY - 1;
			}
		}
		else {
			if (inverted) {
				paletteX = ImportPalette.palette2D_pos.get(0).size() - paletteX - 1;
				paletteY = ImportPalette.palette2D_pos.size() - paletteY - 1;
			}
		}

		} else {
		if (GS.DrawMode) {
			if(ImportPalette.palette2DActivePosition.get(paletteY).get(paletteX)
				&& !GS.flagList[8]){//DrawMode
				return Color.GRAY.getRGB();
			}
		}
		else {
			if(ImportPalette.palette2DActivePosition.get(paletteY).get(paletteX)
					&& !GS.flagList[8]){//DrawMode
					return Color.GRAY.getRGB();
			}
		}
		}
		return ImportPalette.get2DColorByCoordinates(paletteX,paletteY);
		
	}
	
	static <Position> ArrayList<ArrayList<Position>> transpose(ArrayList<ArrayList<Position>> table) {
		ArrayList<ArrayList<Position>> ret = new ArrayList<ArrayList<Position>>();
        final int N = table.get(0).size();
        for (int i = 0; i < N; i++) {
        	ArrayList<Position> col = new ArrayList<Position>();
            for (ArrayList<Position> row : table) {
                col.add(row.get(i));
            }
            ret.add(col);
        }
        return ret;
    }
	
	static <Speeds> ArrayList<ArrayList<Speeds>> transposeSpeed(ArrayList<ArrayList<Speeds>> table) {
		ArrayList<ArrayList<Speeds>> ret = new ArrayList<ArrayList<Speeds>>();
        final int N = table.get(0).size();
        for (int i = 0; i < N; i++) {
        	ArrayList<Speeds> col = new ArrayList<Speeds>();
            for (ArrayList<Speeds> row : table) {
                col.add(row.get(i));
            }
            ret.add(col);
        }
        return ret;
    }
}




