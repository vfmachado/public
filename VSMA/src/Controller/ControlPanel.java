package Controller;

import Model.Event;
import Model.GS;
import View.FieldPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.crypto.spec.GCMParameterSpec;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Controller.update.ElementsManager;
import Controller.update.MyElement;


public class ControlPanel extends JPanel implements ChangeListener, ActionListener, MyElement {

	
	//sliders for time
	private JSlider sliderTime;
	private JLabel lslider;
	private JLabel lsliderInfo;
	
	private JSlider sliderInitialTime;
	private JLabel lInitialTime;
	private JLabel lInitialTimeInfo;
	
	private JSlider sliderFinalTime;
	private JLabel lFinalTime;
	private JLabel lFinalTimeInfo;
	
	
	//track of players
	private JCheckBox track;
	private List ltrack;
	
	private JCheckBox playersVisibility1;
	private JCheckBox playersVisibility2;
	
	private JCheckBox eventsVisibility;
	private JCheckBox currentEventsVisibility;
	
	private JRadioButton rnormalField;
	private JRadioButton rpaletteField;
	private ButtonGroup radios;
	
	private JRadioButton rgeneratePos;
	private JRadioButton rgenerateSpeed;
	private ButtonGroup radiosGenerate;
	
	private JRadioButton rtimeinstant;
	private JRadioButton rtimeinitial;
	private JRadioButton rtimefinal;
	private ButtonGroup radiostime;
	
	private JLabel levents;
	
	private JLabel lspeed;
	private JSpinner spinnerSpeed;
	
	
	private JLabel lselectedEvent;
	private JComboBox<String> btChangeEvent;
	
	private Font font14 = new Font("Calibri", Font.PLAIN, 14);
	
	
//	private JButton btBackPhase;
//	private JButton btNextPhase;
//	private JLabel lbPhases;
	
	
	private JCheckBox cbTimeWindow;
	private int timeDifference = 0;
	private JButton btPreviousTime;
	private JButton btSkipTime;
	
	private boolean changePhase = false;
	
	public ControlPanel() {

		//this.setBackground(Color.gray);

		ElementsManager.addElement(this);
		
		init();
		
	}
	
	
	public void init() {
		
		this.setLayout(null);

		//TIME CONTROL SLIDER
		lslider = new JLabel("Time Instant");
		lslider.setBounds(30, 20, 100, 30);
		this.add(lslider);
		
		sliderTime = new JSlider(0, GS.timeInstantMax -1, 0);
		sliderTime.setBounds(90, 20, 250, 30);
		sliderTime.addChangeListener(this);
		this.add(sliderTime);
		
		lsliderInfo = new  JLabel();
		lsliderInfo.setText("" +GS.timeInstant/60 + ":" + GS.timeInstant%60);
		lsliderInfo.setBounds(350, 20, 60, 30);
		this.add(lsliderInfo);
		
		
		//INTERVAL FIRST SLIDER
		lInitialTime = new JLabel("Initial Time");
		lInitialTime.setBounds(30, 50, 100, 30);
		this.add(lInitialTime);
		
		sliderInitialTime = new JSlider(0, GS.timeInstantMax -1, 0);
		sliderInitialTime.setBounds(90, 50, 250, 30);
		sliderInitialTime.addChangeListener(this);
		this.add(sliderInitialTime);
		
		lInitialTimeInfo = new  JLabel();
		lInitialTimeInfo.setText("" +GS.timeInstant/60 + ":" + GS.timeInstant%60);
		lInitialTimeInfo.setBounds(350, 50, 60, 30);
		this.add(lInitialTimeInfo);
		
		
		//INTERVAL LAST SLIDER
		
		lFinalTime = new JLabel("Final Time");
		lFinalTime.setBounds(30, 80, 100, 30);
		this.add(lFinalTime);
		
		sliderFinalTime = new JSlider(0, GS.timeInstantCurrentMax -1, GS.timeInstantCurrentMax -1);
		sliderFinalTime.setBounds(90, 80, 250, 30);
		sliderFinalTime.addChangeListener(this);
		this.add(sliderFinalTime);
		
		lFinalTimeInfo = new  JLabel();
		lFinalTimeInfo.setText("" +GS.timeInstantCurrentMax/60 + ":" + GS.timeInstantCurrentMax%60);
		lFinalTimeInfo.setBounds(350, 80, 60, 30);
		this.add(lFinalTimeInfo);
		
		
		rtimeinstant = new JRadioButton();
		rtimeinitial = new JRadioButton();
		rtimefinal = new JRadioButton();
		
		rtimeinstant.setBounds(5, 25, 25, 20);
		rtimeinstant.setSelected(true);
		this.add(rtimeinstant);
		rtimeinitial.setBounds(5, 55, 25, 20);
		this.add(rtimeinitial);
		rtimefinal.setBounds(5, 85, 25, 20);
		this.add(rtimefinal);
		
		radiostime = new ButtonGroup();
		radiostime.add(rtimeinstant);
		radiostime.add(rtimeinitial);
		radiostime.add(rtimefinal);
			
		
		rtimeinstant.addActionListener(this);
		rtimeinitial.addActionListener(this);
		rtimefinal.addActionListener(this);
		
		cbTimeWindow = new JCheckBox("Time Freezy");
		cbTimeWindow.setBounds(15, 110, 100, 20);
		cbTimeWindow.setSelected(false);
		cbTimeWindow.addActionListener(this);
		this.add(cbTimeWindow);
		
		
		btPreviousTime = new JButton("<<");
		btPreviousTime.setBounds(140, 110, 60, 25);
		btPreviousTime.addActionListener(this);
		this.add(btPreviousTime);
		
		btSkipTime = new JButton(">>");
		btSkipTime.setBounds(200, 110, 60, 25);
		btSkipTime.addActionListener(this);
		this.add(btSkipTime);

		
		//TRACK CHECK BOX; VALUE AND TOOLTIP
		track = new JCheckBox("Track Players");
		track.setBounds(15, 130, 130, 30);
		track.addActionListener(this);
		this.add(track);
			
		levents = new JLabel("Events:");
		levents.setBounds(160, 130, 300, 30);
		levents.setFont(font14);
		this.add(levents);
		
		
		rnormalField = new JRadioButton("Normal Field Type");
		rpaletteField = new JRadioButton("Palette Field Type");
		
		rnormalField.setBounds(15, 190, 150, 30);
		rpaletteField.setBounds(15, 220, 150, 30);
				
		rnormalField.addActionListener(this);
		rpaletteField.addActionListener(this);
		
		rnormalField.setSelected(true);
		
		radios = new ButtonGroup();
		radios.add(rnormalField);
		radios.add(rpaletteField);
	
		this.add(rnormalField);
		this.add(rpaletteField);
		
		
		
		rgeneratePos  = new JRadioButton("CPM Position Type");
		rgenerateSpeed  = new JRadioButton("CPM Speed Type");
		
		rgeneratePos.setBounds(15, 250, 200, 30);
		rgenerateSpeed.setBounds(15, 280, 150, 30);
		
		rgeneratePos.addActionListener(this);
		rgenerateSpeed.addActionListener(this);
		//rgenerateSpeed.setToolTipText("Max velocity: 9.00 m/s");
		
		rgeneratePos.setSelected(true);
		
		radiosGenerate = new ButtonGroup();
		radiosGenerate.add(rgeneratePos);
		radiosGenerate.add(rgenerateSpeed);
		
		this.add(rgeneratePos);
		this.add(rgenerateSpeed);
		
		
		lselectedEvent = new JLabel("Selected Event:  " + GS.eventsStr.get(GS.selectedAction));
		lselectedEvent.setBounds(15 +150, 320, 200, 30);
		//this.add(lselectedEvent);
		
		
		btChangeEvent = new JComboBox<String>();
		btChangeEvent.setBounds(15, 320, 250, 30);
		btChangeEvent.addActionListener(this);
		this.add(btChangeEvent);
		
		
		for (int i = 0; i < GS.eventsStr.size(); i++)
			btChangeEvent.addItem(GS.eventsStr.get(i));
		
		
		playersVisibility1 = new JCheckBox("Show Team 1");
		playersVisibility1.setBounds(15, 160, 150, 20);
		playersVisibility1.setSelected(true);
		playersVisibility1.addActionListener(this);
		this.add(playersVisibility1);
		
		playersVisibility2 = new JCheckBox("Show Team 2");
		playersVisibility2.setBounds(15 +150, 160, 150, 20);
		playersVisibility2.setSelected(true);
		playersVisibility2.addActionListener(this);
		this.add(playersVisibility2);
		
		eventsVisibility = new JCheckBox("Show Events");
		eventsVisibility.setBounds(260, 190, 150, 20);
		eventsVisibility.addActionListener(this);
		this.add(eventsVisibility);
		
		currentEventsVisibility = new JCheckBox("Current Event");
		currentEventsVisibility.setBounds(260, 210, 150, 20);
		currentEventsVisibility.addActionListener(this);
		currentEventsVisibility.setSelected(true);
		this.add(currentEventsVisibility);
		
		
		
		lspeed = new JLabel("Max Velocity m/s");
		lspeed.setBounds(200, 280, 100, 30);
		this.add(lspeed);
		
		
		spinnerSpeed = new JSpinner(new SpinnerNumberModel(9, 0, 15, 1));
		spinnerSpeed.setBounds(300, 280, 60, 30);
		spinnerSpeed.addChangeListener(this);
		this.add(spinnerSpeed);
	
		infolbA.setBounds(280, 320, 100, 25);
		this.add(infolbA);
		
		infolbB.setBounds(280, 350, 100, 25);
		this.add(infolbB);
		
		
		this.requestFocus();
		sliderTime.requestFocus();
		
	}


	@Override
	public void stateChanged(ChangeEvent evt) {
			
		
		if (evt.getSource() == sliderTime) {
			
			GS.timeInstant = sliderTime.getValue();
			
			String minutes = String.format("%02d", GS.timeInstant/60);
	        String sec = String.format("%02d", GS.timeInstant%60);
	        lsliderInfo.setText("" + minutes + ":" + sec);
			
	       
			//ElementsManager.notifyElements();
			
		}
		
		
		if (evt.getSource() == sliderInitialTime) {
			
			//if (sliderFinalTime.getValue() > sliderInitialTime.getValue()) {
			if (cbTimeWindow.isSelected()) {
			
				GS.timeInstantCurrentMax = sliderInitialTime.getValue() + timeDifference;
				GS.timeInstantCurrentMin = sliderInitialTime.getValue();
				
				if (GS.timeInstantCurrentMax > GS.timeInstantMax)
					GS.timeInstantCurrentMax = GS.timeInstantMax;
				
				
				sliderFinalTime.setValue(GS.timeInstantCurrentMax);
				
			}
			else if (sliderInitialTime.getValue() > sliderFinalTime.getValue()) {
				sliderInitialTime.setValue(sliderFinalTime.getValue() -10);
				GS.timeInstantCurrentMin = sliderFinalTime.getValue() -10;
			}
			else
				GS.timeInstantCurrentMin = sliderInitialTime.getValue();
			
			String minutes = String.format("%02d", GS.timeInstantCurrentMin/60);
	        String sec = String.format("%02d", GS.timeInstantCurrentMin%60);
	        
			lInitialTimeInfo.setText("" + minutes + ":" + sec);
			
			sliderTime.setMinimum(GS.timeInstantCurrentMin);
			sliderTime.setValue(GS.timeInstantCurrentMin);
			
			GS.timeChange = true;
			GS.timePassChange = true;
			
			
							
			//update the system
			//ElementsManager.notifyElements();
			//}
			
		}
		
		if (evt.getSource() == sliderFinalTime) {
			
			//if (sliderFinalTime.getValue() > sliderInitialTime.getValue()) {
			if (sliderInitialTime.getValue() > sliderFinalTime.getValue()) {
				sliderFinalTime.setValue(sliderInitialTime.getValue() +10);
				GS.timeInstantCurrentMax = sliderInitialTime.getValue() +10;
			}
			else {
				GS.timeInstantCurrentMax = sliderFinalTime.getValue();
			}
			
			String minutes = String.format("%02d", GS.timeInstantCurrentMax/60);
	        String sec = String.format("%02d", GS.timeInstantCurrentMax%60);
	        
			lFinalTimeInfo.setText("" + minutes + ":" + sec);
			
			sliderTime.setMaximum(GS.timeInstantCurrentMax);
			
			GS.timeChange = true;
			GS.timePassChange = true;
				
			//update the system
			//ElementsManager.notifyElements();
			this.update();
		//}
		}
		
		if (evt.getSource() == spinnerSpeed) {
			
			GS.maxSpeed = (float) ((Integer)spinnerSpeed.getValue());
			GS.forceCPMupdate = true;
			
			//ElementsManager.notifyElements();
			
		}
		
		
		//TODO Melhorar a forma como isto � feito
		String finalEvents = "Events:";
		for (Event e : GS.eventOnTime(GS.timeInstant)) {
			
			finalEvents += " " + GS.eventsStr.get(e.action);
			
		}
		levents.setText(finalEvents);
		
		
		ElementsManager.notifyElements();
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == btSkipTime) {
			 
			if (GS.timeInstantCurrentMax + timeDifference < GS.timeInstantMax) {
				
				GS.timeInstantCurrentMin += timeDifference;
				GS.timeInstantCurrentMax += timeDifference;
				
				sliderInitialTime.setValue(GS.timeInstantCurrentMin);
				sliderFinalTime.setValue(GS.timeInstantCurrentMax);
				GS.timeChange = true;
				//ElementsManager.notifyElements();
				
				
			}
			
		}
		else if (e.getSource() == btPreviousTime) {
			 
			if (GS.timeInstantCurrentMin - timeDifference >= 0) {
				
				GS.timeInstantCurrentMin -= timeDifference;
				GS.timeInstantCurrentMax -= timeDifference;
				
				sliderInitialTime.setValue(GS.timeInstantCurrentMin);
				sliderFinalTime.setValue(GS.timeInstantCurrentMax);
				GS.timeChange = true;
				//ElementsManager.notifyElements();
				
				
			}
			
		}
		
		
		if (e.getSource() == btChangeEvent) {
			
			GS.selectedAction = btChangeEvent.getSelectedIndex();
						
		}
		
		
		if (e.getSource() == playersVisibility1) {
			
			GS.fieldPlayersEnabled[0] = playersVisibility1.isSelected();
			//ElementsManager.notifyElements();
			
		}
		
		if (e.getSource() == playersVisibility2) {
			
			GS.fieldPlayersEnabled[1] = playersVisibility2.isSelected();
			//ElementsManager.notifyElements();
			
		}
		
		if (e.getSource() == eventsVisibility) {
			
			GS.fieldEventsEnabled = eventsVisibility.isSelected();
			//ElementsManager.notifyElements();
			
		}
		
		if (e.getSource() == currentEventsVisibility) {
			
			GS.fieldCurrentEventEnabled = currentEventsVisibility.isSelected();
			//ElementsManager.notifyElements();
		}
		
		if (e.getSource() == track) {
			
			if (track.isSelected()) {
				GS.trackPlayers = true;
				
				//ElementsManager.notifyElements();
			}
			else {
				GS.trackPlayers = false;
				
				//ElementsManager.notifyElements();
			}
		}
		
		
		if (e.getSource() == rnormalField) {
			
			FieldPanel.normal = rnormalField.isSelected();
			//ElementsManager.notifyElements();
			
		}
		
		if (e.getSource() == rpaletteField) {
			
			FieldPanel.normal = !rpaletteField.isSelected();
			//ElementsManager.notifyElements();
			
		}
		
		if (e.getSource() == rgeneratePos) {
			
			if (rgeneratePos.isSelected()) {
			
				GS.generator = "Position";
				//ElementsManager.notifyElements();
				
			}
		}
		
		if (e.getSource() == rgenerateSpeed) {
			
			if (rgenerateSpeed.isSelected()) {
				
				GS.generator = "Speed";
				//ElementsManager.notifyElements();
				
			}
		}
		
		if (e.getSource() == rtimeinstant     ||
			e.getSource() == rtimeinitial	  ||
			e.getSource() == rtimefinal 
		   ) {
				this.update();
		}
		
		ElementsManager.notifyElements();
		
	}

	private JLabel infolbA = new JLabel();
	private JLabel infolbB = new JLabel();

	@Override
	public void update() {
		
		GS.calculateEventStatistics();
		
		timeDifference = GS.timeInstantCurrentMax - GS.timeInstantCurrentMin;
		
		infolbA.setText("Team A: " + GS.teamAstatistics[btChangeEvent.getSelectedIndex()] + "/" + GS.gameStatistics[btChangeEvent.getSelectedIndex()] );
		infolbB.setText("Team B:" + GS.teamBstatistics[btChangeEvent.getSelectedIndex()] + "/" + GS.gameStatistics[btChangeEvent.getSelectedIndex()] );
		
		sliderTime.setValue(GS.timeInstant);
		
		
		sliderInitialTime.setValue(GS.timeInstantCurrentMin);
		sliderFinalTime.setValue(GS.timeInstantCurrentMax);
		
		if (GS.controlUpdate) {
			sliderTime.setMaximum(GS.timeInstantMax -1);
			sliderFinalTime.setMaximum(GS.timeInstantMax -1);
			sliderFinalTime.setValue(GS.timeInstantMax -1);
			sliderInitialTime.setValue(0);
			sliderInitialTime.setMaximum(GS.timeInstantMax -1);
			GS.controlUpdate = false;
		}
		
		if (rtimeinstant.isSelected())
			sliderTime.requestFocus();
		else if (rtimeinitial.isSelected())
			sliderInitialTime.requestFocus();
		else if (rtimefinal.isSelected())
			sliderFinalTime.requestFocus();
		
		
		
		System.gc();
		
	}
}





