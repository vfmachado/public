package Controller;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import Controller.update.ElementsManager;
import Controller.update.MyElement;
import Model.Event;
import Model.GS;
import View.CPMPanel;

public class CPM implements MyElement {
	
	public static BufferedImage team1CPM_genuine;
	public static BufferedImage team1CPM;
	
	public static BufferedImage team2CPM_genuine;
	public static BufferedImage team2CPM;
	
	public static BufferedImage sortedTeam1CPM;
	public static BufferedImage sortedTeam2CPM;
	
	public static BufferedImage possessionTeam1;
	public static BufferedImage possessionTeam2;
	
	public static BufferedImage drawModePalette;
	
	private int width = GS.width/2;
	private int height = GS.height -100;
	
	//SPEED PALETTE 1D and 2D PALETTE
	private String palette1D_speed_end[] = {	"data/Palettes/1DPalettes/Palette_12.png",
												"data/Palettes/1DPalettes/Palette_17.png",
												"data/Palettes/1DPalettes/Palette_20.png" };
	
	private static String palette2D_end[] = { "data/Palettes/2DPalettes/field_palette_0.png",
								    	"data/Palettes/2DPalettes/field_palette_1.png", 
										"data/Palettes/2DPalettes/field_palette_2.png",
										""} ;
	
	public static int selectedPalette = 1;
	public static int selectedPaletteSpeed = 0;
	public static boolean selectPalettePanel = false;
	//to import palettes (like roger)
	private ImportPalette importPalette = new ImportPalette();
	private VisuGenerator visuGen = new VisuGenerator();
	
	
	//IMAGES
	private BufferedImage imgSpeedPalette = null;
	private BufferedImage soccerPaintField = null;
	
	
	
	
	//used for draw mode
	public static BufferedImage paintPalette;
	
	public CPM() {

		setup();
		
		
		
		ElementsManager.addElement(this);
	}
	
	
	public void draw(Graphics2D g) {
		
		g.setColor(Color.white);
		g.fillRect(0, 0, width, height);
		
		
		
		
		if (GS.sorted == true) {
			
			g.drawImage(sortedTeam1CPM, 0, 0,  width/2-52, height, null);
			g.drawImage(sortedTeam2CPM,   width/2+50, 0, width/2-60, height, null);
			
		}
		else {
			if (GS.cpmAggregation) {
				
				g.drawImage(team1CPM, 0, 0, width/2-52, height, 0, 0, team1CPM.getWidth(), visuGen.cpmSize, null);
				g.drawImage(team2CPM, width/2+50, 0, width, height, 0, 0, team2CPM.getWidth(), visuGen.cpmSize, null);
								
			}
			else {
				g.drawImage(team1CPM, 0, 0, width/2-52, height, null);
				g.drawImage(team2CPM,   width/2+50, 0, width/2-60, height, null);
					
			}
			
			//g.drawImage(possessionTeam1, width/3 +20, 0, 30, height, null);
			//g.drawImage(possessionTeam2, width*2/3 - 50, 0, 30, height, null);
			
			
			//drawMetricLine(g); 
			
			//System.out.println(height + "\t" + GS.timeInstantMax);
			/*
			if (GS.cpmPossessionMode) {
				g.setColor(Color.white);
				
				for (int i = 0; i < GS.timeInstantMax; i++) {
				
					
					int line = (int) ( (float)height/GS.timeInstantMax * i );
					//int line = i;
					/*
					if (GS.possessionMode.equals("Vinicius")) {
						if (GS.possession[i] == 1) {
							g.drawLine(width*2/3 -50, line, width, line);
						}
						else if (GS.possession[i] == 2){
							g.drawLine(0, line, width/3 +50, line);
						}
						/*
						else if  (GS.possession[i] == 0) {
							g.drawLine(width*2/3 -50, line, width, line);
							g.drawLine(0, line, width/3 +50, line);
							
						}
					}
					else
						*/
				/*	{
						if (!GS.inverted) {
							if (GS.possTeam1.get(i) == true) {
								g.drawLine(0, line, width/3 +50, line);
							}
							else {
								g.drawLine(width*2/3 -50, line, width, line);
								
							}
						}
						else {
							if (GS.possTeam1.get(i) == true) {
								g.drawLine(width*2/3 -50, line, width, line);
							}
							else {
								g.drawLine(0, line, width/3 +50, line);
							}
						}
					}
					
				}
				
			}
			*/
		}
		
		
		/* BARS */
		int time = (int) ( (float)height/GS.timeInstantMax * GS.timeInstant );
		g.setColor(Color.black);;
		g.drawLine(0, time, width, time);
		g.drawString("Instant Time", width/2 -30, time);
		
		
		int initaltime = (int) ( (float)height/GS.timeInstantMax * GS.timeInstantCurrentMin );
		g.setColor(Color.green);;
		g.drawLine(0, initaltime, width, initaltime);
		g.drawString("Inital Time", width/2 -30, initaltime);
		
		int finaltime = (int) ( (float)height/GS.timeInstantMax * GS.timeInstantCurrentMax );
		g.setColor(Color.green);;
		g.drawLine(0, finaltime, width, finaltime);
		g.drawString("Final Time", width/2 -30, finaltime);
		
		
		
		g.setColor(GS.team1.getColor());
		g.setStroke(new BasicStroke(4));
		g.drawRect(1, 1, width/2-50, this.height);
		
		g.setColor(GS.team2.getColor());
		g.drawRect(width/2+50, 1, width/2-56, this.height);
		
		if (GS.cpmZoomMode == true) {
			g.setStroke(new BasicStroke(3));
			g.setColor(Color.black);
			g.drawLine(0, CPMPanel.zoomY, width, CPMPanel.zoomY);
			
		}
		
	}
	
	
	//call it when initialize CPM class for each game sample
	public void setup() {
		
		//LOAD PALETTES
		importPalette.fillPalette2D_pos(palette2D_end[selectedPalette]);
		importPalette.fillPalette1D_speed(palette1D_speed_end[0]);
	
		//active positions (all false)
		ImportPalette.fillPalette2DActivePosition();
	
		
		//roger - cria imagem de fundo "data/Palette/Painted_Soccer_Palette.png"
		//acho que nao sera necessário, vamos ver...
		
		
		//load speed_palette
		try {
			imgSpeedPalette = ImageIO.read(new File(palette1D_speed_end[0]));
			//TODO RESIZE IMAGE TO 205, 30
		}
		catch (IOException e) {
			System.out.println("Palette 1D Speed - Image not found");
		}
		
		//initPaintPalette
		soccerPaintField = new BufferedImage(GS.fieldSizeX, GS.fieldSizeY, BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < GS.fieldSizeX; x++) {
			for (int y = 0; y < GS.fieldSizeY; y ++) {
				soccerPaintField.setRGB(x, y, Color.white.getRGB());
			}
		}
		
		
		//paint palette
		loadPaintPalette();
		
		loadCPMs();
		
		
	}
	
	private void loadCPMs() {
		
		System.out.println("Loading CPMs: "  + GS.generator);
		
		visuGen.generateCPMs();
		//visuGen.createPossessionVisualLine();
		
		
	}

	private void loadPaintPalette() {
		
		//TODO: FAZER ISTO
		/*
		 * paintPalette = loadImage("data/Palettes/rainbow_palette.png");
		 * paintPaletteGenuine = loadImage("data/Palettes/rainbow_palette.png");
		 * paintPalette.resize(GS.getFieldX(), GS.getRecSizeY());
		 * 		
		 */
		
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File("data/Palettes/rainbow_palette.png"));
			paintPalette = img;
		}
		catch (IOException e) {
			System.out.println("Paint Palette - Image not found");
		}
		
		
	}
	
	
	public static void loadPaletteFromFile() {
		
		selectedPalette = 3;
		GS.reloadPalette = true;
		
		JFileChooser chooser = new JFileChooser("data");
		chooser.setMultiSelectionEnabled(false);
		
		int value = chooser.showOpenDialog(null);
		
		if (value == JFileChooser.APPROVE_OPTION)
			palette2D_end[3] = chooser.getSelectedFile().getAbsolutePath();
		else
			selectedPalette = 0;	
	}
	
	private void drawMetricLine(Graphics g) {
		
		g.setColor(Color.black);
		
		g.drawLine(width/2, 0, width/2, height);
		
		for (int i = 0; i < GS.timeInstantMax; i+= 300) {
			
			int line = (int) ( (float)height/GS.timeInstantMax * i );
			
			g.drawLine(width/2-10, line, width/2 +10, line);
		}
		
		for (int i = 0; i < GS.timeInstantMax; i+= 60) {
			
			int line = (int) ( (float)height/GS.timeInstantMax * i );
			
			g.drawLine(width/2-5, line, width/2 +5, line);
		}
		
		
		
		
		
		for (Event e : GS.events) {
			
			if (e.action == GS.selectedAction) {
				
				int line = 0;
				
				if (GS.frameRate == 30)
					line = (int) ( (float)height/GS.timeInstantMax * e.frame/30 );
				else
					line = (int) ( (float)height/GS.timeInstantMax * e.frame/7.5 );
				
				
				if (e.success == true)
					g.setColor(Color.green);
				else
					g.setColor(Color.red);
				
				if (!GS.inverted) {
					if (e.team == 0)
						g.fillOval(width/2-15, line-5, 10, 10);
					else
						g.fillOval(width/2+5, line-5, 10, 10);
				}
				else {
					if (e.team == 0)
						g.fillOval(width/2+5, line-5, 10, 10);
					else
						g.fillOval(width/2-15, line-5, 10, 10);
				}
			}
			
		}
		
		
	}

	@Override
	public void update() {

		if (GS.reloadPalette) {
			
			//LOAD PALETTES
			importPalette.fillPalette2D_pos(palette2D_end[selectedPalette]);
			importPalette.fillPalette1D_speed(palette1D_speed_end[selectedPaletteSpeed]);
		
			//active positions (all false)
			ImportPalette.fillPalette2DActivePosition();
		
			GS.reloadPalette = false;
			
			visuGen.generateCPMs();
			
		}
		
		
		if (!VisuGenerator.actualCPM.equals(GS.generator)) {
			visuGen.generateCPMs();
			
		}
			
		
		if (GS.DrawMode) {
			visuGen.generateCPMs();
		}
		
		if (GS.forceCPMupdate) {
			visuGen.generateCPMs();
			GS.forceCPMupdate = false;
		}
		
		
		if (GS.sorted) {
			visuGen.generateCPMs();
			
		}
		
	}


	public static void exportCpm() {

		String cpm1 = new JOptionPane().showInputDialog("CPM Blue Team");
		String cpm2 = new JOptionPane().showInputDialog("CPM Red Team");
		
		JFileChooser chooser = new JFileChooser("data");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setMultiSelectionEnabled(false);
		
		chooser.showOpenDialog(chooser);
		File file = chooser.getSelectedFile();
		String path = file.getAbsolutePath();
		
		BufferedImage image1 = new BufferedImage(450, 1000, BufferedImage.TYPE_INT_RGB);
		BufferedImage image2 = new BufferedImage(450, 1000, BufferedImage.TYPE_INT_RGB);
		
		Graphics g1, g2;
		g1 = image1.getGraphics();
		g2 = image2.getGraphics();
		
		if (GS.sorted) {
			g1.drawImage(sortedTeam1CPM, 0, 0, 450, 1000, 0, 0, sortedTeam1CPM.getWidth(), sortedTeam1CPM.getHeight(),  null);
			g2.drawImage(sortedTeam2CPM, 0, 0, 450, 1000, 0, 0, sortedTeam2CPM.getWidth(), sortedTeam2CPM.getHeight(),  null);
		}
		else{
			g1.drawImage(team1CPM, 0, 0, 450, 1000, 0, 0, team1CPM.getWidth(), team1CPM.getHeight(),  null);
			g2.drawImage(team2CPM, 0, 0, 450, 1000, 0, 0, team2CPM.getWidth(), team2CPM.getHeight(),  null);
		}
		
		System.out.println(path);
		
		
		//export CPMs
		File outputfile = new File(path + "\\" + cpm1 + ".png");
		File outputfile2 = new File(path + "\\" + cpm2 + ".png");
		try {
			ImageIO.write(image1, "png", outputfile);
			ImageIO.write(image2, "png", outputfile2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

		
	
}
