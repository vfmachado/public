package View;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import Controller.CPM;
import Controller.ImportPalette;
import Controller.update.ElementsManager;
import Model.GS;
import View.auxiliar.Area;

public class CPMHelpPanel extends JPanel implements ActionListener, MouseListener {

	private JLabel ltext;
	
	private BufferedImage drawPalette;
	
	private JButton selectPalette;
	private JButton choosePalette;
	private JButton btSaveCPM;
	
	private JButton btCleanDrawMode;
	
	private JCheckBox cDrawMode;
	private JCheckBox cGradient;
	
	private JRadioButton rGradVertical;
	private JRadioButton rGradHorizontal;
	private ButtonGroup groupGrad;
	
	private JCheckBox cbColorToGrad;
	
	private JLabel lselectedColor;
	private Color selectedColor = Color.blue;
	private Color selectedColor2 = Color.red;
	private JCheckBox cZoomMode;
	
	
	
	private JCheckBox cSortModeX;
	private JCheckBox cSortModeY;
	private JCheckBox cSortColumn;
	private JCheckBox cSortLine;
	
	private JCheckBox cPossessionOnly;
	private JCheckBox caggreagate;
	
	private int x1, x2, y1, y2;
	private boolean click1 = false;
	
	private Font font18 = new Font("Calibri", Font.PLAIN, 18);
	
	private ArrayList<Area> areas = new ArrayList<Area>();
	
	private BufferedImage soccerimg = null;
	
	private Graphics g;
	
	public CPMHelpPanel() {
	
		this.setLayout(null);
		
		this.setSize(GS.width/4-30, GS.height-70);
		
		this.addMouseListener(this);
		
		ltext = new  JLabel("CPM Info Panel");
		ltext.setBounds(15, 15, 150, 30);
		ltext.setFont(font18);
		this.add(ltext);
		
		choosePalette  = new JButton("Palette from file");
		choosePalette.setBounds(this.getWidth() -150, this.getHeight()-90, 150, 30);
		choosePalette.addActionListener(this);
		this.add(choosePalette);
		
		btSaveCPM = new JButton("Export CPM");
		btSaveCPM.setBounds(this.getWidth() - 250, this.getHeight() -90, 100, 30);
		btSaveCPM.addActionListener(this);
		this.add(btSaveCPM);
		
		selectPalette = new JButton("Select Palette");
		selectPalette.setBounds(this.getWidth() -150, this.getHeight()-50, 150, 30);
		selectPalette.addActionListener(this);
		this.add(selectPalette);
		
		
		btCleanDrawMode = new JButton("Clean Draw Mode");
		btCleanDrawMode.setBounds(15, this.getHeight()-50, 150, 30);
		btCleanDrawMode.addActionListener(this);
		this.add(btCleanDrawMode);
		
		cDrawMode = new JCheckBox("Draw Mode Enable");
		cDrawMode.setBounds(15, 770, 150, 30);
		cDrawMode.addActionListener(this);
		this.add(cDrawMode);
		
		cGradient = new JCheckBox("Gradient Colors");
		cGradient.setBounds(200, 770, 200, 30);
		cGradient.addActionListener(this);
		this.add(cGradient);
		
		rGradVertical = new JRadioButton("Vertical");
		rGradVertical.setBounds(200, 810, 100, 30);
		rGradVertical.setSelected(true);
		this.add(rGradVertical);
		
		rGradHorizontal= new JRadioButton("Horizontal");
		rGradHorizontal.setBounds(300, 810, 100, 30);
		this.add(rGradHorizontal);
		
		groupGrad = new ButtonGroup();
		groupGrad.add(rGradVertical);
		groupGrad.add(rGradHorizontal);
		
		cZoomMode = new JCheckBox("Zoom Mode Enable");
		cZoomMode.setBounds(15, 930, 150, 30);
		cZoomMode.addActionListener(this);
		this.add(cZoomMode);
		
		cPossessionOnly = new JCheckBox("Non stopped game");
		cPossessionOnly.setBounds(15, 870, 150, 30);
		cPossessionOnly.addActionListener(this);
		this.add(cPossessionOnly);
		
		caggreagate = new JCheckBox("Aggregate");
		caggreagate.setBounds(15, 900, 100, 30);
		caggreagate.setVisible(false);
		caggreagate.setSelected(false);
		caggreagate.addActionListener(this);
		this.add(caggreagate);
		
		
		lselectedColor = new JLabel("Selected Color:");
		lselectedColor.setBounds(15, 800, 150, 30);
		this.add(lselectedColor);
		
		
		cbColorToGrad = new JCheckBox("Grad Color");
		cbColorToGrad.setBounds(15, 840, 100, 30);
		this.add(cbColorToGrad);
				
		
		cSortModeX = new JCheckBox("X Sort");
		cSortModeX.setBounds(200, 850, 100, 30);
		cSortModeX.addActionListener(this);
		this.add(cSortModeX);

		cSortModeY = new JCheckBox("Y Sort");
		cSortModeY.setBounds(200, 880, 100, 30);
		cSortModeY.addActionListener(this);
		this.add(cSortModeY);
		
		
		cSortColumn = new JCheckBox("Column Sort");
		cSortColumn.setBounds(300, 850, 150, 30);
		cSortColumn.setToolTipText("Warning! Player position will lost");
		cSortColumn.addActionListener(this);
		this.add(cSortColumn);
		
		cSortLine = new JCheckBox("Line Sort");
		cSortLine.setBounds(300, 880, 150, 30);
		cSortLine.setToolTipText("Warning! Time line will lost");
		cSortLine.addActionListener(this);
		this.add(cSortLine);
		
		
		try {
			soccerimg = ImageIO.read(new File("data/images/soccerField.png"));
			
		}
		catch (IOException e) {
			System.out.println("Image soccer field - not found");
		}
		
		CPM.drawModePalette = new BufferedImage(this.getWidth()-15, 650, BufferedImage.TYPE_INT_RGB);
		
		for (int y = 0; y < 650; y++) {
			for (int x = 0; x < this.getWidth()-15; x++) {
					CPM.drawModePalette.setRGB(x, y, 16777215);
			}
			
		}
		
		repaint();
		
		//repaint();
		System.out.println("Complete");
		
		
	}
	
	private boolean generateAgain = true;
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.g = g;
		
		g.setColor(Color.gray);
		g.fillRect(15, 50, this.getWidth(), 650);
		//paleta de cores - mouse de 15, width em x; 730 a 760 em y
		g.drawImage(CPM.paintPalette, 15, 730, this.getWidth(), 30, null);
		
		if (selectedColor != null) {
			g.setColor(selectedColor);
			g.fillRect(120, 810, 40, 15);
		}
		
		if (cGradient.isSelected()) {
			g.setColor(selectedColor2);
			g.fillRect(120, 840, 40, 15);
		}
			
		
		Graphics gpalette = CPM.drawModePalette.getGraphics();
		for (Area a : areas) {
			a.draw(gpalette);
		}
		
		//if (areas.size() > 1)
		//	CPM.drawModePalette = CPM.drawModePalette.getSubimage(5, 5, CPM.drawModePalette.getWidth()-10, CPM.drawModePalette.getHeight()-10);
		//soccer image
		g.drawImage(CPM.drawModePalette, 15, 50, this.getWidth(), 700, 0, 0, CPM.drawModePalette.getWidth(), CPM.drawModePalette.getHeight(), null);		
		g.drawImage(soccerimg, 15, 50, this.getWidth(), 700, 0, 0, 900, 1422, null);
		
		if (generateAgain)
			generatePalette();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == selectPalette) {
			
			CPM.selectPalettePanel = true;
				
			ElementsManager.notifyElements();
		}
		else if (e.getSource() == choosePalette) {
			
			CPM.loadPaletteFromFile();
			
			ElementsManager.notifyElements();
			
		}
		else if (e.getSource() == btSaveCPM) {
			
			CPM.exportCpm();
			
		}
			
		
		if (e.getSource() == cDrawMode) {
			
			if (GS.debug) {
				System.out.println(cDrawMode.isSelected() ? "Draw Mode On" : "Draw Mode Disabled" );
			}
			
			GS.forceCPMupdate = true;
			
			GS.DrawMode = cDrawMode.isSelected();
			
			ElementsManager.notifyElements();
			
			
			
		}
		
		
		if (e.getSource() == cZoomMode) {
			
			if (cZoomMode.isSelected())
				GS.cpmZoomMode = true;
			else
				GS.cpmZoomMode = false;
						
		}
		
		
		if (e.getSource() == cPossessionOnly) {
			
			if (cPossessionOnly.isSelected()) {
				GS.onlyPlayingMode = true;
				caggreagate.setVisible(true);		
			}
			else {
				GS.onlyPlayingMode = false;
				caggreagate.setVisible(false);
				caggreagate.setSelected(false);
				GS.cpmAggregation = false;
			}
			GS.forceCPMupdate = true;
			ElementsManager.notifyElements();
		}
		
		if (e.getSource() == caggreagate) {
			
			GS.cpmAggregation = caggreagate.isSelected();
			GS.forceCPMupdate = true;
			ElementsManager.notifyElements();
			
		}
		
		
		if (e.getSource() == cSortModeX) {
			
			if (cSortModeX.isSelected()) {
				GS.sortCPM_X = true;
				//GS.sorted = true;
			}
				
			else {
				GS.sortCPM_X = false;
				
				
			}
			
			GS.forceCPMupdate = true;
			ElementsManager.notifyElements();
		}
		
		
		if (e.getSource() == cSortModeY) {
			
			if (cSortModeY.isSelected()) {
				GS.sortCPM_Y = true;
				//GS.sorted = true;
			}
			else {
				GS.sortCPM_Y = false;
				
				
			}
				
			GS.forceCPMupdate = true;			
			ElementsManager.notifyElements();
		}
		
		
		if (e.getSource() == cSortColumn) {
			
			if (cSortColumn.isSelected()) {
				GS.sorted = true;
				GS.sortCPM_P = true;
			}
			else {
				GS.sortCPM_P = false;
				
				if (!GS.sortCPM_T)
					GS.sorted = false;
				
			}
			GS.forceCPMupdate = true;
			ElementsManager.notifyElements();
			
		}
		
		
		if (e.getSource() == cSortLine) {
			
			if (cSortLine.isSelected()) {
				GS.sorted = true;
				GS.sortCPM_T = true;
			}
			else {
				GS.sortCPM_T = false;
				
				if (!GS.sortCPM_P)
					GS.sorted = false;
				
			}
			
			GS.forceCPMupdate = true;
			ElementsManager.notifyElements();
			
		}
		
		if (e.getSource() == btCleanDrawMode) {
			
			for (int y = 0; y < 650; y++) {
				
				
				for (int x = 0; x < this.getWidth()-15; x++) {
						CPM.drawModePalette.setRGB(x, y, 16777215);
					
				}
				
			}
			
			areas.clear();
			generateAgain= true;
			repaint();
				
		}
		
		repaint();
	}
	
	
	public void generatePalette() {
		
		//CPM.drawModePalette = drawPalette;
		generateAgain = false;
		
		File outputfile = new File("data/GeneratedPaletteDM.jpg");
		try {
			ImageIO.write(CPM.drawModePalette.getSubimage(5, 5, CPM.drawModePalette.getWidth()-10, CPM.drawModePalette.getHeight()-10), "jpg", outputfile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ImportPalette.fillPalette2D_drawMode("data/GeneratedPaletteDM.jpg");
		
		GS.forceCPMupdate = true;
		ElementsManager.notifyElements();
		
	}

	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
		//System.out.println("Mouse Clicked at: " + e.getX() + ", " + e.getY());
		
		//dentro da paleta de cores
		if (e.getX() > 15 && e.getX() < this.getWidth()) {
			
			if (e.getY() > 730 && e.getY() < 760) {
				//Color.
				int color = CPM.paintPalette.getRGB( (e.getX() - 15) * CPM.paintPalette.getWidth()/(this.getWidth()-15), e.getY() - 730);
				
				if (GS.debug) {
					System.out.println("" + color);
				}
				
				if (cbColorToGrad.isSelected())
					selectedColor2 = new Color(color);
				else
					selectedColor = new Color(color);
					
			}
			
		}
		
		
		//click dentro do campo de futebol
		if (e.getX() > 15 && e.getX() < this.getWidth()) {
			
			if (e.getY() > 50 && e.getY() < 700) {
				
				if (click1 == false) {
					
					x1 = e.getX();
					y1 = e.getY();
						
					
					click1 = true;
					
				}
				else {
					
					if (e.getX() < x1) {
						x2 = x1;
						x1 = e.getX();
					}
					else {
						x2 = e.getX();
					}
					
					if (e.getY() < y1) {
						y2 = y1;
						y1 = e.getY();
					}
					else {
						y2 = e.getY();
					}
					
					
					//adiciona a area
					areas.add(new Area(x1-15, y1-50, x2-15, y2-50, selectedColor));
					
					if (cGradient.isSelected()) {
						
						areas.get(areas.size()-1).gradient = true;
						areas.get(areas.size()-1).c2 = selectedColor2;
						
						if (rGradVertical.isSelected())
							areas.get(areas.size()-1).vertical = true;
						else
							areas.get(areas.size()-1).vertical = false;
					}
					
					
					
					click1 = false;
					generateAgain = true;
					repaint();
					
					
				}
				
			}
			
			
		}
		
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
}
