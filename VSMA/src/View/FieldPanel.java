package View;

import Model.Event;
import Model.GS;
import Model.Position;
import View.auxiliar.BackgroundFootball;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.naming.InitialContext;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Controller.update.ElementsManager;
import Controller.update.MyElement;



public class FieldPanel extends JPanel implements MyElement {

	private BackgroundFootball backgroundFootball = new BackgroundFootball();

	public static BufferedImage ballImage;
	
	private Position p;
	
	
	public static boolean normal = true;
	
	public FieldPanel() {
		
		try {
			ballImage = ImageIO.read(new File("data/soccerBall.png"));
		}
		catch (IOException e) {
			JOptionPane.showMessageDialog(null, "No image found");
		}
		
		ElementsManager.addElement(this);
			
		this.setVisible(true);
		repaint();
	}

	
	@Override
	protected void paintComponent(Graphics g) {
	
		long initialTime = System.currentTimeMillis();
		
		//if (GS.debug)
		//	System.out.println("Updating field panel - DEBUG MODE");
		
		Graphics2D g2 = (Graphics2D) g.create();
		
		RenderingHints rh = new RenderingHints(
	             RenderingHints.KEY_ANTIALIASING,
	             RenderingHints.VALUE_ANTIALIAS_ON);
	    g2.setRenderingHints(rh);
		
		backgroundFootball.paint(g2, normal);
	
		
		Position pb = GS.ballPositions.get(GS.timeInstant);
		g2.drawImage(ballImage, (int) pb.resized().x - 10, (int) pb.resized().y - 10,(int) pb.resized().x + 10, (int) pb.resized().y + 10, 0, 0, 32, 32, null);
		
		
		if (GS.fieldPlayersEnabled[0] == true) {
			for (int i = 0; i < GS.team1.size(); i ++) {
				
				GS.team1.getPlayer(i).draw(g2);
				
			}
		}
		
		if (GS.fieldPlayersEnabled[1] == true) {
			for (int i = 0; i < GS.team1.size(); i ++) {
				
				GS.team2.getPlayer(i).draw(g2);
				
			}
		}
		/*
		if (GS.fieldEventsEnabled == true) {
			for (Event e: GS.eventOnTime(GS.timeInstant)) {
					
					
					Position p = e.pos.resized();
					
					if (e.team == 0) {
						if (e.success == true)
							g.setColor(GS.team1.getColor().darker());
						else
							g.setColor(GS.team1.getColor().brighter());
						
						g.fillOval((int)p.x-10, (int)p.y-10, 20, 20);
					}
					else {
						if (e.success == true)
							g.setColor(GS.team2.getColor().darker());
						else
							g.setColor(GS.team2.getColor().brighter());
						
						g.fillOval((int)p.x-10, (int)p.y-10, 20, 20);
					
					
				}
				
			}
		}
		*/
		if (GS.fieldEventsEnabled == true) {
			for (Event e : GS.events) {
				
				if (e.action == GS.selectedAction) {
					
					Position p = e.pos.resized();
					
					if (e.team == 0) {
						if (e.success == true)
							g.setColor(GS.team1.getColor().darker());
						else
							g.setColor(GS.team1.getColor().brighter());
						
						g.fillOval((int)p.x-5, (int)p.y-5, 10, 10);
					}
					else {
						if (e.success == true)
							g.setColor(GS.team2.getColor().darker());
						else
							g.setColor(GS.team2.getColor().brighter());
						
						g.fillOval((int)p.x-5, (int)p.y-5, 10, 10);
					}
					
				}
				
			}
		}
		
		if (GS.fieldCurrentEventEnabled == true) {
			
			for (Event e : GS.eventOnTime(GS.timeInstant)) {
				
				Position p = e.pos.resized();
				
				if (e.team == 0) {
					if (e.success == true)
						g.setColor(GS.team1.getColor().darker());
					else
						g.setColor(GS.team1.getColor().brighter());
					
					g.fillOval((int)p.x-5, (int)p.y-5, 10, 10);
				}
				else {
					if (e.success == true)
						g.setColor(GS.team2.getColor().darker());
					else
						g.setColor(GS.team2.getColor().brighter());
					
					g.fillOval((int)p.x-5, (int)p.y-5, 10, 10);
				}
			}
			
			
			
			g2.setColor(Color.white);

			if (GS.eventOnTime(GS.timeInstant).size() > 0) {
			
				p = GS.eventOnTime(GS.timeInstant).get(0).pos.resized();
			}	
			
				int time = GS.timeInstant+1;
				
				while (GS.eventOnTime(time).size() == 0) {
					time++;
				}
				
				if (time < GS.timeInstantMax && p != null) {
				
					Position q = GS.eventOnTime(time).get(0).pos.resized();
					
					g2.setStroke(new BasicStroke(3));
					
					g2.drawLine((int)p.x, (int)p.y, (int)q.x, (int)q.y);
					
					g2.setStroke(new BasicStroke());
				}
			
		}
		
		
		ArrayList<Point> posicoest1 = new ArrayList<Point>();
		ArrayList<Point> posicoest2 = new ArrayList<Point>();
		
		for (int i = 0; i < GS.numberOfPlayers; i++) {
			
			if (GS.team1.getPlayer(i).getPosition(GS.timeInstant).y > -1000) {
				
				posicoest1.add(new Point((int) GS.team1.getPlayer(i).getPosition(GS.timeInstant).resized().x,
									     (int) GS.team1.getPlayer(i).getPosition(GS.timeInstant).resized().y)
							);
				
			}
			
			if (GS.team2.getPlayer(i).getPosition(GS.timeInstant).y > -1000) {
				
				posicoest2.add(new Point((int) GS.team2.getPlayer(i).getPosition(GS.timeInstant).resized().x,
									     (int) GS.team2.getPlayer(i).getPosition(GS.timeInstant).resized().y)
							);
				
			}
			
		}
		
		
		
//		Position p = GS.ballPositions.get(GS.timeInstant);
//		g2.drawImage(ballImage, (int) p.resized().x - 10, (int) p.resized().y - 10,(int) p.resized().x + 10, (int) p.resized().y + 10, 0, 0, 128, 128, null);
//		
		//System.out.println(p.x + "\t" + p.y);
		
		
		//TODO TACTICAL LINES IN MAIN VIEW OF THE FIELD
		//team 1
		
		
		//if (GS.possTeam1.get(GS.timeInstant - GS.timeInstantCurrentMin)) {
		if (!GS.inverted) {
			if (GS.possession[GS.timeInstant] == 1) {
				g2.setColor(GS.team1.getColor());
				g2.drawString("BALL", 350, 20);
			}
			else if (GS.possession[GS.timeInstant] == 2) {
				g2.setColor(GS.team2.getColor());
				g2.drawString("BALL", 350, 590);
			}
			else {
				g2.setColor (Color.white);
				g2.drawString("STOPPED GAME", 160, 300);
			}
		}
		else {
			if (GS.possession[GS.timeInstant] == 2) {
				g2.setColor(GS.team1.getColor());
				g2.drawString("BALL", 350, 20);
			}
			else if (GS.possession[GS.timeInstant] == 1) {
				g2.setColor(GS.team2.getColor());
				g2.drawString("BALL", 350, 590);
			}
			else {
				g2.setColor (Color.white);
				g2.drawString("STOPPED GAME", 160, 300);
			}
		}
		

		
		//long delay = System.currentTimeMillis() - initialTime;
		//System.out.println("Time spend to print soccer field: " + delay);
	}
	
	@Override
	public void update() {
		
		repaint();
		
	}
	
	
}
