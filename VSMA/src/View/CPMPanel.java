package View;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import Controller.CPM;
import Controller.update.ElementsManager;
import Controller.update.MyElement;
import Model.GS;
import View.auxiliar.SelectPalettePanel;
import View.auxiliar.ZoomPanel;

public class CPMPanel extends JPanel implements MyElement, MouseMotionListener, MouseListener {

	private CPM cpm;
	private ZoomPanel zoomPanel;
	private SelectPalettePanel selectPalette;
	public static int zoomY = 0;
	public CPMPanel() {
		
		ElementsManager.addElement(this);	
		
		this.setLayout(null);
		this.setBounds(0, 0, GS.width/2, GS.height -70);
		this.setVisible(true);
		
		cpm = new CPM();
	
		zoomPanel = new ZoomPanel( );
		zoomPanel.setSize(this.getWidth()/2-50, 300);
		zoomPanel.setVisible(false);
		this.add(zoomPanel);
		
		
		selectPalette = new SelectPalettePanel();
		selectPalette.setSize(600,400);
		selectPalette.setLocation(this.getWidth()/2 - 300, this.getHeight()/2 - 200);
		selectPalette.setVisible(false);
		this.add(selectPalette);
		
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		repaint();
	}

	@Override
	public void update() {
		
		if (GS.debug)
			System.out.println("CPM Repaint");
		
		this.repaint();
		
		
		
	}
	
	
	@Override
	protected void paintComponent(Graphics g) {
		
		cpm.draw((Graphics2D) g.create());
		
		if (CPM.selectPalettePanel == true) {
			selectPalette.setVisible(true);
		}
		
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		
		
		if (GS.cpmZoomMode == true) {
			
			zoomY = e.getY();
			
			//System.out.println("ZOOOM");
			if (e.getX() < this.getWidth()/2) {
				zoomPanel.setZoomPosition(e.getX(), (int) ( (float)CPM.team1CPM.getHeight()/this.getHeight() * e.getY() ));
				zoomPanel.setLocation(e.getX() - zoomPanel.getWidth()/2, e.getY() - zoomPanel.getHeight()/2);
				zoomPanel.setVisible(true);
			}
			else {
				zoomPanel.setZoomPosition(e.getX(), (int) ( (float)CPM.team2CPM.getHeight()/this.getHeight() * e.getY() ));
				zoomPanel.setLocation(e.getX() - zoomPanel.getWidth()/2, e.getY() - zoomPanel.getHeight()/2);
				zoomPanel.setVisible(true);
			}
			
			this.repaint();
				
		}
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
		if (CPM.selectPalettePanel == true) {
			
			if (e.getY() > this.getHeight()/2 - 200 && e.getY() < this.getHeight()/2 + 200) {
		
				if (e.getY() < this.getHeight()/2 + 100) {
					
					if (e.getX() < this.getWidth()/2 - 100)
						CPM.selectedPalette = 0;
					else if (e.getX() < this.getWidth()/2 + 100)
						CPM.selectedPalette = 1;
					else
						CPM.selectedPalette = 2;
				}
				else if (e.getY() > this.getHeight()/2 + 100)  {
					
					if (e.getX() < this.getWidth()/2 - 100)
						CPM.selectedPaletteSpeed = 0;
					else if (e.getX() < this.getWidth()/2 + 100)
						CPM.selectedPaletteSpeed = 1;
					else
						CPM.selectedPaletteSpeed = 2;
					
				}
				
				selectPalette.setVisible(false);
				GS.reloadPalette = true;
				CPM.selectPalettePanel = false;
				
				ElementsManager.notifyElements();
			}
		}	
		else {
			System.out.println( e.getY());
			GS.timeInstant = (int) ( (float)GS.timeInstantMax/this.getHeight() * e.getY() );
			
			ElementsManager.notifyElements();
		}
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
		zoomPanel.setVisible(false);
		zoomY = -10;
		this.repaint();
	}

	

	
}
