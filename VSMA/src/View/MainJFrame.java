package View;

import Model.GS;


import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import Controller.ControlPanel;


public class MainJFrame  extends JFrame implements ActionListener {

	/**
	 * 	JFrame is a serializable class
	 */
	private static final long serialVersionUID = 1L;

	
	private JLabel lmatchinfo;
	
	private JScrollPane scrollPanel = new JScrollPane();
	
	private JPanel bigPanel;
	
	//tipos de fonte utilizados
	private Font font10 = new Font("Calibri", Font.PLAIN, 10);
	private Font font14 = new Font("Calibri", Font.PLAIN, 14);
	private Font font18 = new Font("Calibri", Font.PLAIN, 18);
	
	private JComboBox<String> listGames;
	
	public static FieldPanel field;
	
	private JLabel lteam1;
	private JLabel lteam2;
	private JLabel lteamNo;
	//Constructor
	public MainJFrame() {

		
		init();

	}
	

	//Init
	private void init() {
		
		
		this.setTitle("Football Analysis");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		/this.setResizable(false);
		//this.setLayout(null);
		
		
		bigPanel = new JPanel();
		bigPanel.setPreferredSize(new Dimension(GS.width, GS.height));
		bigPanel.setBounds(0, 0, GS.width, GS.height);
		bigPanel.setLayout(null);
		
		
		scrollPanel.setViewportView(bigPanel);
		scrollPanel.getVerticalScrollBar().setUnitIncrement(16);
		setContentPane(scrollPanel);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		//this.setSize(GS.width, GS.height);
		if (screenSize.width >= 1920) {
			this.setSize(GS.width, GS.height);
			
		}
		else {
			this.setSize(screenSize);
			
		}
				
		
		
		//LOOK AND FEEL - Use system interface
		try {
            // Set System L&F
	        UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	       // handle exception
	    }
	    catch (ClassNotFoundException e) {
	       // handle exception
	    }
	    catch (InstantiationException e) {
	       // handle exception
	    }
	    catch (IllegalAccessException e) {
	       // handle exception
	    }
		
		
		lmatchinfo = new JLabel("");
		lmatchinfo.setFont(font18);
		lmatchinfo.setBounds(GS.width/2 - 150, 5, 300, 25);
		//this.add(lmatchinfo);
		bigPanel.add(lmatchinfo);
		
				
		field = new FieldPanel();
		field.setBounds(30, 30, GS.fieldSizeX, GS.fieldSizeY);
		//this.add(field);
		bigPanel.add(field);
		
		ControlPanel control = new ControlPanel();
		control.setBounds(30, GS.fieldSizeY + 60, GS.fieldSizeX, GS.height - GS.fieldSizeY - 100);
		//this.add(control);
		bigPanel.add(control);
		
		CPMHelpPanel infoPanel = new CPMHelpPanel();
		infoPanel.setBounds(GS.width*3/4 , 30, GS.width/4- 30, GS.height - 70);
		//this.add(infoPanel);
		bigPanel.add(infoPanel);
		
		CPMPanel setOfPanels = new CPMPanel();
		setOfPanels.setBounds(GS.fieldSizeX + 60, 30, GS.width/2, GS.height - 70);
		//this.add(setOfPanels);
		bigPanel.add(setOfPanels);
		
	
		listGames = new JComboBox<String>();
	
		
		listGames.addItem("CapSpoT1");	//ok
		
	
		
		String game = listGames.getSelectedItem().toString();
		
		if (GS.inverted) {
			
			lmatchinfo.setText("Match Info: " + game.substring(3, 6).toUpperCase() + " x " + game.substring(0, 3).toUpperCase());
			lmatchinfo.setText("<html>Match Info: <font color=blue>" + game.substring(3, 6).toUpperCase() + "</font>" + " x " + "<font color=red>" +game.substring(0, 3).toUpperCase() + "</font></html>"); 
					
			//lmatchinfo.setText("<html><font color=red>RED</font> - <font color=navy>Navy</font></html>");
		}
		else {
			
			lmatchinfo.setText("Match Info: " + game.substring(0, 3).toUpperCase() + " x " + game.substring(3, 6).toUpperCase());
			lmatchinfo.setText("<html>Match Info: <font color=blue>" + game.substring(0, 3).toUpperCase() + "</font>" + " x " + "<font color=red>" +game.substring(3, 6).toUpperCase() + "</font></html>");
			
		}
		
		listGames.setBounds(0, 0, 150, 30);
		listGames.addActionListener(this);
		//this.add(listGames);
		bigPanel.add(listGames);
		
		
		lteam1 = new JLabel("");
		lteam2 = new JLabel("");
		lteamNo = new JLabel("");
		
		lteam1.setBounds(170, 5, 100, 30);
		lteam2.setBounds(270, 5, 100, 30);
		lteamNo.setBounds(370, 5, 100, 30);

		bigPanel.add(lteam1);
		bigPanel.add(lteam2);
		bigPanel.add(lteamNo);
		
		
		bigPanel.setVisible(true);
		
		//this.add(bigPanel);
		this.setVisible(true);
	
		
		String minutes = String.format("%02d", GS.team1Poss/60);
        String sec = String.format("%02d", (GS.team1Poss%60));
		lteam1.setText("Team 1: " + minutes + ":" + sec);
		
		minutes = String.format("%02d", GS.team2Poss/60);
        sec = String.format("%02d", (GS.team2Poss%60));
		lteam2.setText("Team 2: " + minutes + ":" + sec);
		
		minutes = String.format("%02d", GS.teamNoPoss/60);
        sec = String.format("%02d", (GS.teamNoPoss%60));
		lteamNo.setText("Stopped: " + minutes + ":" + sec);
	}


	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == listGames) {
			
			String game = listGames.getSelectedItem().toString();
			
			
			GS.loadData(game);
			
			if (GS.inverted) {
				
				lmatchinfo.setText("Match Info: " + game.substring(3, 6).toUpperCase() + " x " + game.substring(0, 3).toUpperCase());
				lmatchinfo.setText("<html>Match Info: <font color=blue>" + game.substring(3, 6).toUpperCase() + "</font>" + " x " + "<font color=red>" +game.substring(0, 3).toUpperCase() + "</font></html>"); 
						
				//lmatchinfo.setText("<html><font color=red>RED</font> - <font color=navy>Navy</font></html>");
			}
			else {
				
				lmatchinfo.setText("Match Info: " + game.substring(0, 3).toUpperCase() + " x " + game.substring(3, 6).toUpperCase());
				lmatchinfo.setText("<html>Match Info: <font color=blue>" + game.substring(0, 3).toUpperCase() + "</font>" + " x " + "<font color=red>" +game.substring(3, 6).toUpperCase() + "</font></html>");
				
			}
			
			String minutes = String.format("%02d", GS.team1Poss/60);
	        String sec = String.format("%02d", (GS.team1Poss%60));
			lteam1.setText("Team 1: " + minutes + ":" + sec);
			
			minutes = String.format("%02d", GS.team2Poss/60);
	        sec = String.format("%02d", (GS.team2Poss%60));
			lteam2.setText("Team 2: " + minutes + ":" + sec);
			
			minutes = String.format("%02d", GS.teamNoPoss/60);
	        sec = String.format("%02d", (GS.teamNoPoss%60));
			lteamNo.setText("Stopped: " + minutes + ":" + sec);
			
			
		}
		
	}



	public static void main(String[] args) {
			
		
		GS.loadData("CapSpoT1");
		
		MainJFrame main = new MainJFrame();
		
	}
}
