package View.auxiliar;

import Model.GS;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import Controller.ImportPalette;



public class BackgroundFootball {
	
	private BufferedImage img = null;
	private BufferedImage img2 = null;
	private BufferedImage img3 = null;
	public BackgroundFootball() {
		
		try {
			img = ImageIO.read(new File("data/images/soccerField.png"));
			img2 = ImageIO.read(new File("data/images/soccerField2.png"));
			img3 = ImageIO.read(new File("data/images/soccerField3.png"));
		}
		catch (IOException e) {
				System.out.println("Image not found");
		}
		
	}
	
	public void paint(Graphics2D g, boolean normal) {
		
		if (normal) {
			g.setColor(Color.green.darker());
			g.fillRect(0, 0, GS.fieldSizeX, GS.fieldSizeY);
		}
		
		else {
			
			g.drawImage(ImportPalette.palette2dImg, 0, 0, GS.fieldSizeX, GS.fieldSizeY, null);
					
		}
		
		g.drawImage(img, 0, 0, GS.fieldSizeX, GS.fieldSizeY, 0, 0, 900, 1422, null);
		
		
	}
	
	
	public void paint(Graphics2D g, int sx, int sy, int tx, int ty) {
		
		//g.fillRect(tx, ty, GS.fieldSizeX, GS.fieldSizeY);
		//g.drawImage(img2, tx, ty, tx +sx, ty+sy, 0, 0, 100, 158, null);
		g.drawImage(img, tx, ty, tx +sx, ty+sy, 0, 0, 900, 1422, null);
		
	}
	
public void paint2(Graphics2D g, int sx, int sy, int tx, int ty) {
//		g.setColor(Color.green);
//		
//		g.fillRect(tx, ty, sx, sy);
			
		g.drawImage(img2, tx, ty, tx +sx, ty+sy, 0, 0, 100, 158, null);
		
	}
	
}
