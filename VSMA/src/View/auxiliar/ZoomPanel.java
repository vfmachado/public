package View.auxiliar;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import Controller.CPM;
import Model.GS;

public class ZoomPanel extends JPanel {

	private int x = 0;
	private int y = 0;
	
	public void setZoomPosition(int x, int y) {
		
		this.x = x;
		this.y = y;
		repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		
		g.setColor(Color.blue);
		g.fillRect(0, 0, 100, 0100);
		
		if (x < 480) {
			
			if (GS.sorted)
				g.drawImage(CPM.sortedTeam1CPM, 0, 0, this.getWidth(), this.getHeight(), 0, y - 50, CPM.sortedTeam1CPM.getWidth(), y + 50, null);
			else
				g.drawImage(CPM.team1CPM, 0, 0, this.getWidth(), this.getHeight(), 0, y - 50, CPM.team1CPM.getWidth(), y + 50, null);
			
		}
		else {
			if (GS.sorted)
				g.drawImage(CPM.sortedTeam2CPM, 0, 0, this.getWidth(), this.getHeight(), 0, y - 50, CPM.sortedTeam2CPM.getWidth(), y + 50, null);
			else
				g.drawImage(CPM.team2CPM, 0, 0, this.getWidth(), this.getHeight(), 0, y - 50, CPM.team2CPM.getWidth(), y + 50, null);
		}
		
		
		g.setColor(Color.black);
		g.drawLine(0, this.getHeight()/2-1, this.getWidth(), this.getHeight()/2-1);
		g.drawLine(0, this.getHeight()/2,   this.getWidth(), this.getHeight()/2);
		g.drawLine(0, this.getHeight()/2+1, this.getWidth(), this.getHeight()/2+1);
		
		
	}
	
	
}
