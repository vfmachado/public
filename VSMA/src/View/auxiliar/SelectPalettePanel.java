package View.auxiliar;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;



public class SelectPalettePanel extends JPanel {

	private BufferedImage img0 = null;
	private BufferedImage img1 = null;
	private BufferedImage img2 = null;
	private BufferedImage img3 = null;
	private BufferedImage img4 = null;
	private BufferedImage img5 = null;
	
	
	public SelectPalettePanel() {

		
		//open image
		
		try {
			img0 = ImageIO.read(new File("data/Palettes/2DPalettes/field_palette_0.png"));
			img1 = ImageIO.read(new File("data/Palettes/2DPalettes/field_palette_1.png"));
			img2 = ImageIO.read(new File("data/Palettes/2DPalettes/field_palette_2.png"));
			img3 = ImageIO.read(new File("data/Palettes/1DPalettes/Palette_12.png"));
			img4 = ImageIO.read(new File("data/Palettes/1DPalettes/Palette_17.png"));
			img5 = ImageIO.read(new File("data/Palettes/1DPalettes/Palette_20.png"));
		}
		catch (IOException e) {
			System.out.println("Palette 2D Image not found");
		}
		
		
		repaint();

	}
	
	
	@Override
	protected void paintComponent(Graphics g) {
		// TODO PAREI AQUI
		super.paintComponent(g);
		
		this.requestFocus();
		
		g.drawImage(img0, 0, 0, 200, 300, null);
		g.drawImage(img1, 200, 0, 200, 300, null);
		g.drawImage(img2, 400, 0, 200, 300, null);
		
		g.setColor(Color.white);
		g.drawRect(0, 0, 200-1, 300-1);
		g.drawRect(200, 0, 200-1, 300-1);
		g.drawRect(400, 0, 200-1, 300-1);
		
		
		g.drawImage(img3, 0, 300, 200, 100, null);
		g.drawImage(img4, 200, 300, 200, 100, null);
		g.drawImage(img5, 400, 300, 200, 100, null);
		
		g.setColor(Color.white);
		g.drawRect(0, 300, 200-1, 100-1);
		g.drawRect(200, 300, 200-1, 100-1);
		g.drawRect(400, 300, 200-1, 100-1);
		
	}



	
}
