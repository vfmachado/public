package View.auxiliar;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Area {

	public int x1, x2, y1, y2;
	public Color c;
	
	public boolean gradient = false;
	public Color c2;
	public boolean vertical = true;
	
	public Area(int x1, int y1, int x2, int y2, Color c) {
		
		this.c = c;
		
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		
		
	}
	
	public void draw(Graphics g) {
		
		if (!gradient) {
			g.setColor(c);
			g.fillRect(x1, y1, x2-x1, y2-y1);
		}
		else {
			
			GradientPaint gp;
			
			if (vertical)
				gp = new GradientPaint( (x1+x2)/2, y1, c, (x1+x2)/2, y2, c2);
			else
				gp = new GradientPaint(x1, (y1+y2)/2, c, x2, (y1+y2)/2, c2);
			
			Graphics2D g2 = (Graphics2D)g.create();
			g2.setPaint(gp);
			g2.fillRect(x1, y1, x2-x1, y2-y1);
			
		}
	}
	
}
